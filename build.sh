#!/usr/bin/env bash

echo_color() {
	if [[ $? -ne 0 ]]; then
		clear='\033[0m'

		# red
		color='\033[0;31m'
		echo -e "\n${color}Error : $1${clear}\n"
		exit 1
	else
		clear='\033[0m'

		# green
		color='\033[0;32m'
		echo -e "\n${color}$1${clear}\n"
	fi

}

echo_color "Building the Hugo site..."
hugo --minify --cleanDestinationDir || echo_color "Hugo build failed"

echo_color "Pushing changes to Git repository..."
git push origin main || echo_color "Couldn't push changes to upstream repository"

echo_color "Moving into the 'public' directory..."
cd public/ || echo_color "Couldn't change directory to 'public'"

echo_color "Current directory: $(pwd)/"

echo_color "Adding changes to Git repository..."
git add -A
git commit -m "Auto Commit: Site Updated"
git push origin main || echo_color "Couldn't push changes to source repository"

echo_color "Site updated successfully."
