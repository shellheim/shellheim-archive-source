# The Shell Archives Source

This repository houses all the source code for the website
[The Shell Archives](https://shellheim.codeberg.page/)

You can find the codeberg pages repository
[here.](https://codeberg.org/shellheim/pages)

## Development Notes to Self

**1. Clone the repo**

```git
git clone git@codeberg.org:shellheim/shellheim-archive-source.git
```

**2. Submodules**

Initialize the submodule (so the theme can work)

```git
git submodule update --init --recursive
```

Update the submodules (read update theme)

```git
 git submodule update --remote --merge
```

**3. Publishing Flow**

I have set up a system where the `public/` directory is the git repo for the
codeberg pages repo, so that when I update the site with `hugo` the changes
immediately reflect in the `public/` directory and thus on the actual website.

First remove the hugo `public/` directory with

```sh
rm public/ -rf
```

Then clone the pages repo into the new `public/` directory with

```git
git clone git@codeberg.org:shellheim/pages.git public/
```

**4. Build.sh**

Now, after doing all that you can just make your changes, add them and run
`build.sh`, which executes this:

```sh
hugo || exit 1
git push origin main || echo "Couldn't push source to upstream" && exit 1
cd public/ || exit 1
git add -A
git commit -m "Auto Commit, Site Updated"
git push origin main || echo "Failed to push, site built." && exit 1
```

The `build.sh` file makes sure that :

(i) the repo is pushed to `main`.

- So after adding all the changes you can just run ./build.sh without pushing
  yourself.

(ii) the site is built with the command `hugo`

(iii) all the changes in `public/` because of the previous `hugo` command are
added with `git add -A`

Then it pushes this pages repo (in the `/public` directory) to remote and the
website updates on https://shellheim.codeberg.page

## Screenshots

### Homepage

<p align="center">
<img src="./static/images/screenshots/homepage.webp" />
</p>

---

### Post

<p align="center">
  <img src="./static/images/screenshots/post.webp" />
</p>
