#!/bin/bash

find_imgs=$(find ./static/images/ -type f \( -name "*.png" -o -name "*.jpg" -o -name "*.jpeg" -o -name "*.ppm" \) -not -path "./static/images/favicons/*")

for file in $find_imgs; do
	ffmpeg -i "$file" -compression_level 5 "${file%.*}.webp"
	rm "$file"
done

if [ $? = 0 ]; then
	echo "Images Converted"
else
	echo "Failed to Convert Images."
fi
