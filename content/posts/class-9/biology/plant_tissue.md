---
title: "Plant Tissues"
date: 2023-09-20T18:24:52+05:30
draft: false
author: Shellheim
tags: ["Biology", "Science", "Tissue", "Plant tissue", "Cell"]
---

Tissues are groups of cells with similar structure, composition and functions. They are an intermediary between basic cells and complete organs.

The hierarchy of components of living (multicellular) organism goes like this :

```goat

Cell < Tissue < Organ < Organ System < Organism

```

## Types of Plant Tissues

Plants are of different kinds in shape, size, habitat etc. but all of them have similar needs on a basic level. Thus, plants have different kinds of tissues to serve those needs. Some plants have a particular tissue more than the other or in different arrangement than others.

{{< figure align=center src="/images/biology/tissue/diagram.svg" alt="Diagram Showcasing Different Types of Plant Tissues" caption="Diagram Showcasing Different Types of Plant Tissues" >}}

**There are two types of plant tissues.**

1. Meristematic Tissue

2. Permanent Tissue

### Meristematic Tissue

**The tissues which posses the capability to perform cell division continuously throughout their lifetime are called meristematic tissue.**

{{< figure align=center src="/images/biology/tissue/meristem.webp" alt="Diagram Showcasing Different Types of Plant Tissues" caption="Meristematic Tissue. Courtesy [Encyclopædia Britannica](https://www.britannica.com/science/meristem#/media/1/376101/6567) " >}}

They perform cell division continuously and are responsible for the [primary](#apical-meristem) and [secondary](#lateral-meristem) growth of the plant.

According to their occurrence on the plant body, they are divided into three types:

1. Apical Meristem

2. Lateral Meristem

3. Intercalary Meristem

#### Apical Meristem

The meristematic tissue situated at the tip (apical from 'apex' meaning top) of the plant, i.e., on the tip of the roots and shoots of the plant are called apical meristem.

These tissues are responsible for what's called the `primary growth` of the plant. They help the shoot of the plant grow which increases the plant's height and they also help the roots of the plant grow which helps the plant in getting more water and minerals deep underground.

#### Lateral Meristem

The meristematic tissue situated at side of the plant's stem are called lateral meristem. (Lateral means of the side.)

Lateral meristem helps the plant to grow laterally. It increases the plant stem's girth or thickness. This growth of the stem's thickness is called it's `secondary growth.`

#### Intercalary Meristem

Before talking about this, let me explain nodes and internodes.
Nodes are the points on a stem where buds, twigs, leaves etc. originate.
Internode is the space between such nodes.

{{< figure align=center src="/images/biology/tissue/internodes.webp" alt="A plant with nodes and internodes labelled" caption="Nodes and internodes in a plant" >}}
These meristematic tissue are present on the node and on internodes between other permanent tissues.
They are responsible for the vertical growth of the internode and consequently, of the whole plant.

{{< figure align=center src="/images/biology/tissue/meristem_types.webp" alt="Diagram Showcasing Different Types of Plant Tissues" caption="Types of meristematic tissues" >}}

### Permanent Tissue

Cells which lose the ability to divide and take up specific shapes and functions are called permanent tissues.

These types of tissues are also called `differentiated tissues.` The process of losing the ability to divide and taking up a specific shape and function is called `differentiation.`

According to their nature of cells, they are classified into two categories:

1. Simple Permanent Tissue

2. Complex Permanent Tissue

## Simple Permanent Tissue

Permanent tissues which contain a single type of cell having similar structure and functions are called simple permanent tissue.

Simple permanent tissue are divided into three types:

1. Parenchyma

2. Collenchyma

3. Sclerenchyma

### Parenchyma

Parenchyma is the most abundant type of tissue in the plant body.

- It consists of thin-walled, unspecialised, living cells.

- It's cells appear to be spherical, polygonal or elongated in shape.

- The cells contain large vacuoles for storage purposes.

- The cells walls of parenchyma cells are thin and made up cellulose.

- Parenchyma has loosely packed cells, thus, it has large intercellular spaces between adjacent cells.

#### Types of Parenchyma

According to their composition, parenchyma tissues are classified into two categories:

1. Chlorenchyma : Parenchyma tissues with cells containing chlorophyll are called chlorenchyma.

2. Aerenchyma : Partially submerged plants like water lily, lotus etc. have their top parts floating above the water. They have parenchyma tissues filled with air with increases their buoyancy and allows them to float, these air filled parenchyma are called aerenchyma.

#### Functions of Parenchyma

- Storage of food materials.

- When it contains chlorophyll (chlorenchyma), it performs photosynthesis.

- Aerenchyma tissues provide buoyancy to the aerial parts of a partially submerged plant and make them float.

{{< figure align=center src="/images/biology/tissue/parenchyma.webp" alt="Image showing parenchyma tissue" caption="Parenchyma tissue" >}}

### Collenchyma

Collenchyma is a type of simple permanent tissue with living cells that mainly provides flexibility to the plant body with a bit of mechanical support.

- It consists of elongated cells which appear polygonal in the cross-section.

- Collenchyma cells have irregular deposition of cellulose, hemicellulose and pectin at the corners of their cell walls. This makes their cell walls irregularly thick at the corners.

- Collenchyma tissue have compactly arranged cells with no intercellular space.

#### Location of Collenchyma

Collenchyma tissues generally occur below the epidermis (outermost layer) of stems, below the leafstalk (petiole) and in the mid-ribs of a few plants.

#### Functions of Collenchyma

- It's main function is to provide flexibility to young plant parts and specialized parts like tendrils in creepers or stems in climbers.

- To provide tensile strength which increases the flexibility of stems of the stem and branches.

- A few collenchyma contain chlorophyll and perform photosynthesis to manufacture food.

### Sclerenchyma

Sclerenchyma tissues consist of dead cells that don't have living protoplasm.

- Sclerenchyma have elongated, narrow cells which look polygonal in the cross-section.

- Their cells have thick walled cells made up of lignin, this makes the cell wall hard and impermeable (which eventually kills the cell).

- These tissues have very closely packed cells which doesn't leave any intercellular space between cells.

- Excessive thickening of cell walls in sclerenchyma cells greatly reduces the space inside the cell (called lumen).

#### Types of Sclerenchyma

Sclerenchyma has two types:

1. Sclereids

2. Fibres

#### Location of Sclerenchyma

It's found in both aerial and underground parts of plants.

- Fibres are present in the stem around the vascular bundle and in the veins of leaves.

- Fibres are also found in husks of fruits like coconut.

- Sclereids are found in the hard covering of nuts like walnut, pistachio etc.

#### Functions of Sclerenchyma

- To provide the maximum mechanical strength to the mature parts of plants.

- Strength and rigidity provided by sclerenchyma helps the plant maintain external pressure.

> Note : Sclerenchyma also have some economical value because things like flax, jute, ropes and other different textiles can be made from them.

{{< figure align=center src="/images/biology/tissue/sclerenchyma.webp" alt="Image showing sclerenchyma tissue" caption="Sclerenchyma tissue" >}}

## Complex Permanent Tissue

Permanent plant tissues which are composed of more than one type of cell but perform a common, specific function are called complex permanent tissues.

They are also called `vascular tissue` and together `vascular bundle` because they help in the transportation of water and distribution of food.

According to what they conduct, they have been divide into two types :

1. Xylem

2. Phloem

## Xylem

The vascular tissue which performs the upward conduction of water (unidirectionally) is called xylem.

They xylem tissue itself has four components:

### 1. Xylem Vessle

- These are long tubes that form a row of cylindrical cells which are end-to-end attached to each other.
- The partition of horizontal cell walls in the cells are either dissolved partially or completely to achieve continuity and aid the conduction of water.

- These are dead cells with hard, lignified cell walls.

#### Functions of xylem vessles

- Xylem vessles help in the longitudinal conduction of water.

### 2. Tracheids

- These are elongated cells with tapering ends and large cavities.

- Tracheids are dead cells with hard, lignified cell walls.

#### Functions of Tracheids

- Helps in the lateral/horizontal conduction of water & also provides mechanical support due to it's lignified cell walls.

### 3. Xylem Parenchyma

Xylem Parenchyma are just normal parenchyma cells that are part of the vascular bundle.

- Only living element in xylem tissue.

- Elongated in shape and are stacked on one another.

#### Functions of Xylem Parenchyma

- Primary function of xylem parenchyma is to store water and food.

- Also helps in the lateral conduction of water.

- Provides mechanical support to the plant.

### 4. Xylem Sclerenchyma

Xylem Sclerenchyma are, just as xylem parenchyma, the same tissues as their name suggests.

Their only function is to provide mechanical support. They are, as previously mentioned, made up of dead cells.

## Phloem

The vascular tissue which performs the conduction of food materials (bidirectionally) is called phloem.

The Phloem has four components:

### Sieve Tubes

- Sieve tubes are elongated, thin-walled, living cells cells which are shaped like tubes.

- Sieve tube cells don't have a nucleolus but are still considered living.

- The last walls of a sieve tube is called an `end wall`, these end walls are porous to facilitate conduction.

- Stacked on top of each other (arranged vertically).

- Sieve plates (end walls when stacked) are present.

#### Functions of Sieve Tubes

- The main function of sieve tubes is the conduction of food (such as glucose) from production site to (and from) others parts of the plant.

### Companion Cell

- Thin walled, living parenchymatous cells with dense cytoplasm.

- Exists mainly to assist the sieve tube.

#### Functions of Companion Cells

- Assist the sieve tube in the conduction of food.

- Provide the sieve tube with water if the xylem tissue is unable to do so.

- Provide energy to sieve tubes for active transport of food.

- Helps in the storage of food into phloem parenchyma.

### Phloem Parenchyma

- Phloem parenchyma are made up of normal, thin-walled, living parenchyma cells.

- Exists outside of the xylem and phloem tissues.

#### Functions of Phloem Parenchyma

- The primary function of phloem parenchyma is to store food that doesn't need to be conducted immediately by the sieve tubes.

### Phloem Sclerenchyma

- This is the only dead component of the four components of phloem tissue.

- It's composed of dead sclerenchyma cells with thick, lignified cell walls and narrow internal spaces (lumen).

#### Function of Phloem Sclerenchyma

- It's only function is to provide maximum mechanical strength to the phloem tissue.
