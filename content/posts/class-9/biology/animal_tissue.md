---
title: "Animal Tissues"
date: 2024-02-29T21:22:07+05:30
draft: false
---

This post is tied to the post I made about [plant tissues.](/posts/class-9/biology/plant_tissue)

Animal tissues are classified into a LOT of categories and sub-categories. I am not going to do that. This will cover the general types and will not divide that particular type's constituents into sub-types of any kind. Well then, let's start!

## Classification of Animal Tissues

### Epithelium Tissue

It's the first type of animal tissue in _most_ classifications. It's a **layer of cells on the entire body surface and acts a barrier to separate different body systems.**

It's a continuous sheet of tightly packed cells. The cells are attached with a non-cellular basement membrane which is composed of collagen protein.

#### Functions

1. **Protection**

It protects the underlying cells from adverse environmental conditions such as dryness, injury and bacteria.

2. **Barrier** As written before, epithelium forms an effective barrier and separates different body systems.
3. **Exchange of Material**

The permeability of epithelium tissue plays an important role in the exchange of materials between the body and the external environment.

4. **Absorption**

The epithelial tissue of alimentary canal helps in the absorption of water and nutrients.

5. **Secretion**

Few epithelial tissues are capable of secreting various substances like sweat, saliva, enzymes etc.

6. **Nerve Conduction**

Some epithelial cells help in nerve transmission, like taste buds on the tongue and the receptors present in the ear.

## Types of Epithelium Tissues

### 1. Squamous Epithelial

- It's made up of thin layers of flat cells.
- It consists of smooth but irregular cells.
- Each cell has a spherical nucleus at the center of the cytoplasm.

#### Location

It forms the delicate inner lining of :

- blood vessels
- the heart
- alveoli
- lungs
- food pipe

#### Functions

- acts as a protective lining
- acts as a selectively permeable surface
- secretes biologically important molecules

> Note : The human skin is differently developed, which shows multiple layers of epithelial cells and called stratified squamous tissue.

### 2. Cuboidal Epithelium

This is the only epithelium with multiple layers. It consists of cube like cells, which appear polygonal in section. These cells have spherical and centrally located located nucleus.

#### Location

Cuboidal epithelium forms the inner linings of :

- kidney tubules
- duct of sweat gland
- duct of salivary gland
- alveoli

and inner layers of the ovary and thyroid.

#### Functions

- It's a mechanical tissue so it provides structural strength.
- Helps in absorption, exchange of materials and secretion.

### 3. Columinar Epithelium

It consists of `pillar-like` cells, these cells are tall and thin giving them this appearence. These cells have an oval nucleus located at the base of the cell. The epithelium may have striated borders containing microvilli at the free end of the cell which increases the surface area.

#### Location

The columinar epithelium cells are present in the lining of :

- stomach
- intestine
- gall bladder
- oviduct.

#### Functions

- The layer of columinar cells facilitate the movement of substance across itself.

- Absorption

### 4. Gandular Epithelium

Consists of — columinar or specialized cuboidal — cells capable of releasing substances. These cells form
the [glands](https://en.wikipedia.org/wiki/Gland) in the human body. The isolated glandular cells are known as `unicellular glands` while a cluster of such cells are known as `multicellular glands.`

The glands which **secrete their synthesized substances directly into the bloodstream** are called **endocrine glands** while the glands which **secrete** substances **by** way of a **duct** are called **exocrine glands.**

#### Location

It's present in glands like :

- the pancreas
- adrenal glands
- mamary gland

#### Functions

- The exocrine glands secrete mucus, saliva, oil, milk and other enzymes.
- The endocrine gland secretes hormones which are responsible for carrying out different, vital body functions. An example would be the pituitary gland.

### 5. Ciliated Epithelium

Certain cuboidal or columinar cells bear numerous hair-like structures called `cillia` on their surface.

#### Location

They are present in :

- the ovidcut
- the sperm duct
- respiratory tract

etc.

#### Functions

The cillia of the epithelium tissue helps in moving particles in a specific direction by moving rythmically.
