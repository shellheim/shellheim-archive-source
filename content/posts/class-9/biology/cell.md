---
title: 'Cells'
date: 2023-10-29T16:38:31+05:30
draft: false
tags:
    [
        'Biology',
        'Science',
        'Cell',
        'Plant Cell',
        'Animal Cell',
        'Nucleus',
        'Golgi Body',
        'Photosynthesis',
    ]
---

**The most basic and functional unit of life is called a cell.** These cells
make up every organism on earth. First discovered by
[Robert Hooke](https://en.wikipedia.org/wiki/Robert_Hooke) when examined some
[cork](https://en.wikipedia.org/wiki/Robert_Hooke#Microscopy), these were named
`cells` because their structure looked like
[honeycomb cells.](https://archive.org/details/micrographiaorso00hook/page/113/mode/1up)

These cells are not functional on their own but need several `organelles` to do
the heavy lifting. Each organelle has different functions but these come
together to serve the common purpose of the cell.

I am going to document these organelles in detail, from their structure,
functions to their correspondence with other cell organelles. But first we need
to look at `cell theory`, which is the basis of modern biology.

{{< figure align=center src="/images/biology/cell/animal_cell.webp" alt="A diagram of an animal cell" caption="An Animal Cell. Courtesy : [Encyclopædia Britannica](https://www.britannica.com/science/cell-biology#/media/1/101396/112877)" >}}

## Cell Theory

Two German scientists,
[Matthias Schleiden](https://en.wikipedia.org/wiki/Matthias_Schleiden) and
[Theodor Schwann](https://en.wikipedia.org/wiki/Theodor_Schwann) worked together
and made discoveries about cells and their role in all organisms.

Matthias Schleiden first proposed in 1839 that every plant is made up cells,
while Schwann extended this statement to animals in the same year. However, they
both disagreed on how cells come to be. Schleiden believed that cells came from
`free cell formation`, i.e., they crystallized into existence out of nowhere.
Schleiden's claim was refuted by
[Rudolf Virchow](https://en.wikipedia.org/wiki/Rudolf_Virchow) who gave the idea
that **cells came from pre-existing cells.** Virchow is believed to have
[plagiarized](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1646803/) the work of
a polish scientist [Robert Remak](https://en.wikipedia.org/wiki/Robert_Remak).

These gave us the three principles of cell theory.

### Principles of Cell Theory

1. All organisms are composed of one or more cells.
2. The cell is the most basic unit of structure and organisation in organisms.
3. Cells come from pre-existing.

## Classification of Cells

Cells, on the basis of their nuclear material, are divided into two types :

**1. Prokaryotic Cells** - Cells which don't possess a well-defined nucleus and
organelles are called `prokaryotes`. Here `pro` means primitive/before in Greek
and karyon means kernel, so`pro + karyon = prokaryote (primitive cell)`.

Prokaryotic cells are generally cells of bacteria, viruses, fungi or other such
single-celled organisms like amoeba.

**2.Eukaryotic Cells** - Cells which have a well-defined, membrane-bound nucleus
and membrane-bound organelles are called eukaryotes. Here `eu` means well/good
in Greek and—as previously mentioned—`karyon` means kernel, so
`eu + karyon = eukaryote ( Well defined cell)`.

Eukaryotic cells are generally cells of complex, multi-cellular organisms like
monkeys.

{{< figure align=center src="/images/biology/cell/prokaryote_cell.svg" alt="A diagram of a prokaryotic cell" caption="Diagram of a Prokaryotic Cell">}}

### Difference in Prokaryotes and Eukaryotes

Sourced from
[wikipedia.](<https://en.wikipedia.org/wiki/Cell_(biology)#Eukaryotic_cells>)

|                   | Prokaryotes                               | Eukaryotes                                         |
| ----------------- | ----------------------------------------- | -------------------------------------------------- |
| **Size**          | ~1-5 μm                                   | ~10-100 μm                                         |
| **Nucleus**       | Nucleoid, no true nucleus                 | contains true, double membrane-bound nucleus       |
| **Organism**      | Typically single celled                   | single as well as multi-cellular                   |
| **Cell Division** | Binary Fission                            | Mitosis / Meiosis                                  |
| **Organelles**    | No specialized, membrane-bound organelles | specialized, membrane-bound organelles are present |

## Classification of Eukaryotic Cells

Eukaryotic cells are further divided according to the type of organism they
belong to. These are of two types:

1. Animal Cells

2. Plant Cells

### Animal Cells and Plant Cells

Eukaryotic cells belonging to members of the animal kingdom are called
`animal cells.`  
Similarly, cells belonging to members of the plant kingdom are called
`plant cells.`

{{< figure align=center src="/images/biology/cell/plant_cell_and_animal_cell.webp" alt="Comparison of a plant cell and animal cell side by side." caption="Comparison of Plant and Animal Cells. Courtesy : [Encyclopædia Britannica](https://www.britannica.com/science/cell-biology#/media/1/101396/107000)"  >}}

This diagram shows both an animal cell and a plant cell side by side. It may be
observed that both have most of the organelles in common (some with a little
difference) and a few unique ones. These organelles are what power these cells.

## Cell Components

I will categorize others as "components" because they don't quite fit the
category of organelles. These are :

1. Cell Membrane
2. Cell Wall
3. Cytoplasm

### Cell Membrane

The outermost-in animal cells and the innermost in plant cells- layer is called
the `cell membrane.` It's a selectively permeable membrane, as in it allows some
substances such as {{< rawhtml >}}H<sub>2</sub>O{{< /rawhtml >}},
{{< rawhtml >}}CO<sub>2</sub>{{< /rawhtml >}} etc., pass while blocking
undesirable objects like hostile organisms.

{{< figure align=center src="/images/biology/cell/bi_lipid_layer.webp" alt="A diagram of the bilipid layers of a cell membrane" caption="Cell Membrane Layers" >}}

#### Structure of the Cell Membrane

The cell membrane is made up of two layers of phospholipids, also called a
`bi-lipid` layer, which have proteins between them.

The proteins which fully penetrate the bi-lipid layer are called `intrinsic`
proteins, while the ones which only fully penetrate one of the two layers are
called `extrinsic` proteins.

#### Functions of the Cell Membrane

-   Intrinsic proteins help in the exchange of substances to and from the cell.
-   Extrinsic proteins while not fully penetrating the bi-lipid layer, it also
    helps in transport of materials in times of distress. It also alerts the
    cell of the dangers outside like viruses, hostile bacteria etc.

### Cell Wall

The outermost boundary of plant, algae and fungi cells is called a `cell wall.`

-   It's a dead component.
-   Has high tensile strength.
-   It's freely permeable.

Plant cells are composed of cellulose, hemicellulose and pectin.

### Cytoplasm

The area inside the cell is filled with a viscous, gelatinous fluid which is
called cytoplasm. It helps in the transport of cellular substances like
proteins, lysosomes etc. inside the cell.

#### Composition of Cytoplasm

| Substance                 | %   |
| ------------------------- | --- |
| Oxygen                    | 62  |
| Carbon                    | 20  |
| Hydrogen                  | 10  |
| Nitrogen                  | 3   |
| Calcium                   | 2.5 |
| Other essential materials | 2.5 |

#### Parts of Cytoplasm

1. Cytosol - The translucent fluid in which cell organelles are suspended, it
   make up 70% of the cytoplasm.

2. Cytoskeletal - The fibrous strands which give the cell a proper shape, adjust
   the cell organelles position and aid in the transport of vesicles. These are
   only found in eukaryotes.

#### Functions of Cytoplasm

-   To give the cell a proper shape via the cytoskeletal.

-   To help in dissolving in water-soluble substances.

-   It aids in the transfer of cellular material inside the cell.

## Cell Organelles

Cell organelles are sub-units of a cell which generally have a specialized
function. There are quite a few of these, each with a different structure, size,
function etc. The organelles covered here are going to be:

1.  Common organelles

    -   [Nucleus](#nucleus)
    -   [Endoplasmic Reticulum](#endoplasmic-reticulum)
    -   [Golgi Apparatus](#the-golgi-apparatus)
    -   [Mitochondria](#mitochondria)
    -   [Lysosome](#lysosome)
    -   [Vacuole](#vacuole)

2.  Organelles specific to plant cells

    1. [Plastids](#plastids)

        - [Chloroplast](#chloroplast)
        - [Chrmoplast](#chromoplast)
        - [Leucoplast](#leucoplast)

    2. Cell Wall

### Nucleus

The nucleus is a spherical, double membrane-bound organelle located at the
periphery in plant cells and at the center in animal cells, it's the most
important cell organelle in the cell as it controls all cellular functions.

{{< figure align=center src="/images/biology/cell/nucleus.webp" alt="A diagram of nucleus " caption="A Nucleus Diagram" >}}

-   The nucleus is what differentiates eukaryotes and prokaryotes.
-   It controls the metabolic activities of the cell.

#### Nuclear Membrane

This is a double layered membrane which separates the nucleus from the cytoplasm
of the cell. This protects the cell from foreign objects and organisms.

#### Nuclear Envelopes & Pores

Nucleus pores are small openings in the nuclear membrane, these allow the entry
and exit of macromolecules like ribosome. Envelopes are then, the pieces of the
dashed membrane.

#### Chromatin

Long threads made up of DNA and proteins carrying genetic data necessary for
cell division. These condense into gene-carrying chromosomes during cell
division.

#### Nucleolus

The spherical structure present at the center of the nucleus. It's not membrane
bound and is responsible for the synthesis of ribosome.

### Endoplasmic Reticulum

The Endoplasmic Reticulum is a contains membrane organelle made up of flattened
sacs, covering the space around the nucleus.

It's found in most eukaryotic cells.

{{< figure align=center src="/images/biology/cell/endoplasmic_reticulum.webp" alt="A Endoplasmic Reticulum Diagram" caption="Endoplasmic Reticulum. Courtesy : [Encyclopædia Britannica](https://www.britannica.com/science/endoplasmic-reticulum#/media/1/187020/114952)" >}}

According to appearance, the E.R. is divided into two types :

1. Rough Endoplasmic Reticulum

Named so due to appearance caused by embedded ribosomes which enable it to make
protein.

2. Smooth Endoplasmic Reticulum

Not having embedded ribosomes gives this part of the E.R. a `smooth` appearance,
due to the lack of ribosomes, it manufactures lipids.

#### Functions of the Endoplasmic Reticulum

1. Under Rough Endoplasmic Reticulum

    1. Synthesis of protein with the help of the ribosomes.
    2. Modification of proteins.
    3. Transportation of proteins to different sites of the cell via packaging
       in vesicles.

2. Under Smooth Endoplasmic Reticulum

    1. Synthesis of lipids.
    2. Transportation of lipidids.
    3. Secretion of hormones via the endocrine glands.
    4. Detoxification and filtering of consumed substances, specially during
       alcohol and drug overdose.

### The Golgi Apparatus

The Golgi Apparatus or the golgi apparatus is a membrane-bound cell organelle
made up of flat, stacked sacs called `cistarnae`. It's found in most eukaryotic
cells.

{{< figure align=center src="/images/biology/cell/golgi_apparatus.webp" alt="A golgi apparatus Diagram" caption="Golgi Body. Courtesy : [Encyclopædia Britannica](https://www.britannica.com/science/Golgi-apparatus#/media/1/238044/114953)" >}}

#### The Golgi Network

It has three components and two networks, namely:

1. Components

-   **Cis** : The face closest to the E.R.

-   **Medial** : Middle part of the golgi apparatus, located at medium distance
    from the E.R.

-   **Trans** : The face closest to the E.R.

2. Networks

-   **Cis Network**

-   **Trans Network**

The networks belong to their respective components and are composed of multiple
cistarnae. Here's [a youtube video](https://www.youtube.com/watch?v=Jn-1lB5jb6I)
explaining the networks in detail.

#### Functions of the Golgi Apparatus

The proteins and lipids manufactured by the E.R. go into the golgi apparatus to
be :

**1. Modified** : It modifies the proteins/lipids by adding other components
such as other types of proteins and lipids.

**2. Packaged & Transported** : It packages its modified proteins/lipids into
vesicles and transports them across the cell.

Since the vesicles forms out of the golgi itself, its essentially always
forming.

### Mitochondria

As you most probably already know, mitochondria
[is the powerhouse of the cell.](https://www.youtube.com/watch?v=vhRpLSwxKdw)

It's a vital organelle for eukaryotes (it's only found in eukaryotic cells),
it's double membrane-bound, it uses oxygen (through aerobic respiration) to
dissolve nutrients into energy that's usable by the cell, into adenosine
triphosphate or ATP.

{{< figure align=center src="/images/biology/cell/mitochondria.webp" alt="A diagram of a mitochondria" caption="Mitochondria. Courtesy : [Encyclopædia Britannica](https://www.britannica.com/science/mitochondrion#/media/1/386130/17869)" >}}

#### Structure of the Mitochondria

It's `bean` shaped with two membranes -

1. Inner Membrane

2. Outer Membrane

The inner membrane is folded towards the inside making a `finger-like` structure
called `cristae`. These folds host a particle which is essential to the
production of ATP. These are called `F1 particles` or oxysomes. That's the
reason these finger-like folds exist, having these increases the surface area on
which the F1 particles are hosted, therefore increasing the ATP production.

It also houses a matrix in the inner membrane which is a liquid containing
different types of enzymes, ribosomes and other proteins.

### Lysosome

Lysosome is a membrane-bound organelle present in many eukaryotes. It carries
various digestive enzymes inside its membrane for varied purposes. Its structure
is quite simple, it's just a membrane-bound spherical object filled with
digestive enzymes.

#### Functions of the Lysosome

1. To digest foreign organisms which enter the cell.

2. To digest harmful foreign substances.

3. To release digestive enzymes into the cell if it becomes diseased and may
   harm surround cells, this is also why it's called **suicidal bag.**

### Vacuole

Vacuoles are some of the more noticeable category of organelles. They exist in
both animal cells and plant cells. They're generally formless and
expand/contract as the cell needs it to. They are far bigger in plant cells than
their animal cell counterparts, as can be seen
[here.](#animal-cells-and-plant-cells)

#### Functions of Vacuoles

They are mostly containers for different types of storage.

-   Isolating materials that might be harmful or a threat to the cell.
-   Containing waste products.
-   Containing water in plant cells.
-   Maintaining internal hydrostatic pressure or turgor within the cell during
    osmosis.

### Plastids

Plastids are a group of double membrane-bound organelles found only in plant
cells and algae. We'll cover only **3 types of plastids** out of the eight.

They are :

**1. Chromoplast**

**2. Chromoplast**

**3. Leucoplast**

#### Chromoplast

These kinds of plastids contain a non-green pigment (usually brownish to
reddish), they have irregular shapes and they don't change into other types of
plastids.

-   They give color to the non-green parts of a plant like the flowers, fruits,
    roots etc.

#### Chloroplast

Chloroplasts are perhaps the most important type of plastids. They contain
`chlorophyll`, which gives plants their distinctive green color and enables them
to manufacture food via `photosynthesis.`

{{< figure align=center src="/images/biology/cell/chloroplast.webp" alt="A diagram of a chloroplast" caption="A Chloroplast Visualized in 3D. Courtesy : [Encyclopædia Britannica](https://www.britannica.com/science/chloroplast#/media/1/113761/45552)" >}}

The above diagram shows a chloroplast with the following :

**1. Thylakoid** : These are membrane-bound compartments which are shaped like
sacs, (Thylakoid comes from Greek `thylakos`, which means `sac`) they are the
sites of photosynthesis.

**2. Grana** : A bunch thylakoids when stacked together are called grana
(singular : granum).

**3. Stroma** : Is the homogeneous in which grana exist.

**4. Lamella** : Lamella simply connects different grana.

#### Leucoplast

These are colorless, that is, they contain no pigment. They are spherical in
shape and **store food like starch, lipids and protein.**

#### Functions of Plastids

**1. Chloroplast**

-   Perform photosynthesis to manufacture food.
-   Give color to leaves, shoots, stem etc.

**2. Chromoplast**

-   Give color to flowers, fruits, roots etc. to attract animals for pollination
    and/or seed dispersal.

**3. Leucoplast**

-   To store starch, lipids and protein.

Until next time!
