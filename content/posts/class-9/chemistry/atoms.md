---
title: 'Atoms'
date: 2024-04-02T16:10:06+05:30
draft: false
---

The present structure of the atom is widely known. But how it got to where it is
today is quite interesting. This journey includes many experiments and models.
I'll be going over them in chronological order and try to describe their history
and contents as best as I can.

This post covers things related to Dalton's theory, another posts will cover the
next experiments and models.

## Terminology

1. **Atom :** From the Greek `atomos` (uncuttable), smallest unit of a chemical
   element.

2. **Element :** The smallest possible unit of matter that cannot be decomposed
   into simpler substances by ordinary chemical processes.

3. **Compound :** The substance made from the combination of identical molecules
   of two or more elements in fixed proportion.

4. **Molecule :** Generally, a group of two or more atoms chemically bonded
   together. It's also defined as **'The smallest particle of a substance that
   is capable of an independent existence with all the properties of that
   substance'.**

5. **Atomic Mass :** The mass of an atom relative to {{< rawhtml >}} <math>
   <mfrac> <mi>1</mi> <mi>12</mi> </mfrac> </math> {{< /rawhtml >}}th of the
   mass of Carbon-12. That is, the masss of one atom of carbon-12.

6. **Atomic Number :** Number of protons found in the nucleus of an atom. Symbol
   (Z).

7. **Mass Number :** Number of total nucleons present in the nucleus of an atom,
   e.g., number of protons + neutrons.

## John Dalton's Atom

Dalton imagined atoms as sort billiard balls. As in — solid, uniform in shape
and composition and indivisible in nature (though that maybe disproven on a
billiard ball with enough force).

### Dalton's Atomic Theory.

Now these gradually became the core tenets of **Dalton's Atomic Theory.**

```
1. Everything (elements) is composed of extremely small particles called atoms.

2. All atoms of a given element are identical in all respects & atoms of a different element are different in all respects (yup, different masses too).

3. Atoms are neither created nor destroyed in chemical reactions.

4. Compounds are formed when atoms of more than one element combine. A given compound always has the same relative number and kind of atoms.

5. Compounds always form when atoms combine in the ratio of simple whole numbers, e.g., 2:3 and 1:8 etc.

6. Atoms of elements may combine in different ratios to form different compounds.

7. An atom is the smallest particle that takes part in a chemical reaction.

```

{{< figure align=center src="/images/chemistry/atoms/hydrogen.svg" alt="Hydrogen's orbital structure" caption="An atom of hydrogen" >}}

## Molecules and Compounds

Many elements have different types of molecules. Some elements like Argon (Ar)
have only one atom in their molecule and are thus, called `monoatomic`. Some
elements like Oxygen (O) have two atoms of oxygen in their molecule and are
called `diatomic`. The total number of atoms in the molecule of an element or
compound is called the **atomicity** of that element or compound.

The naming generally goes to `tetra-atomic` and to `poly-atomic`.

## Compounds and Their Proportions

Atoms join together in definite proportions to form compounds. For example,
water ({{< rawhtml >}}H<sub>2</sub>O{{< /rawhtml >}}) has the mass ratio of 1:8.
Since every water molecule has 4 hydrogen atoms for 2 oxygen atoms, the hydrogen
atoms' mass is {{< rawhtml >}} <math> 2 × 4 = 8 </math> and the oxygen atoms'
mass is <math>2 × 16 = 32</math> and <math>4 : 32 = 1 : 8</math>
{{< /rawhtml >}}. This means, no matter where you get your water from it'll
always have {{< rawhtml >}} <math><mfrac><mi>1</mi>
<mi>8</mi></mfrac></math>{{< /rawhtml >}} atoms of hydrogen by weight.

## Laws of Chemical Reactions

### Law of Conservation of Mass

This law was given by Antoine Lavoisier with experimental proof. This law states
that **the total mass of reactants will be equal to the total mass of the
products, in a chemical reaction.**

{{< rawhtml >}}

<div style="margin: 0 0 12px 0;" >
    <math style="font-size:1.6em;" >A + B ⟶ C + D </math>
</div>

{{< /rawhtml >}}

where Mass of (A + B) = Mass of (C + D).

### Law of Constant Proportion

This law was also put forward by Lavoisier, it states that **in a chemical
substance the elements are always present in definite proportions by mass.**

This was done because scientists at the time observed that all compounds would
always have the same of amount of atoms by weight. For example, the mass ratio
of hydrogen and oxygen was always found to be 1 : 8.

## Valency

Every atom has multiple electron orbits around it. They are called orbitals `K`,
`L`, `M` and `N`. Each orbital is sometimes represented as `n`. So, if we're
talking about `K` we'll take n = 1, if we're talking about `L`, we'll take n = 2
and so on.

Now, since protons have a positive charge and electrons have a negative charge,
they cancel out each other. Every atom that is neutral in charge has the name
number of both protons and electrons. But how are these electrons arranged?

{{< rawhtml >}} <strong> Each orbital (almost always) has <math>

2<msup><mi>n</mi><mn>2</mn></msup> </math> amount of electrons. </strong>

<!-- Hangul Fillers -->

That is, if we're talking about orbital `M`, n = 3. Then, the no. of electrons
in shell `M` will be <math> 2 × <msup><mi>ㅤ ㅤ3</mi><mn>2</mn></msup>ㅤ = 18
</math> {{< /rawhtml >}}.

{{< figure align=center  src="/images/chemistry/atoms/electron_orbitals.webp" alt="Covalent Bond Diagram" caption="Two chlorine atoms one electron away from an octet state" >}}

### Octet State

Each atom seeks to have 8 electrons in its outermost shell, also called the
**valence shell.** However, this octet rule doesn't apply to atoms that may lose
their outer electrons to accommodate the maximum number of electrons in their
next shell. For example, if helium has 2 electrons in it's `K` shell then it's
valency is 0 because this shell can only accommodate 2 electrons.

Similarly, hydrogen's valency is 1 because it only needs one more electron to
reach it's limit of electrons (limit of 1) for it's first shell. These won't
acquire more electrons in their other (non-existent) shells because their
nucleus only has protons for that many electrons. That is, they don't tend to
acquire charge.

Now, atoms up to Boron work this way where they just lose their outermost
electrons to keep maximum number of electrons in their immediate inner shell.
After that atoms want to acquire electrons to have 8 electrons in their
outermost shell.

As a general rule of thumb, if the number of electrons in the outermost shell is
≤ 3, the valency is just that number, that is, 3. If the number of electrons in
the outermost shell is ≥ 4, then the valency is calculated the simple arithmetic
of 8 - no. of electrons in outermost shell.

For example,

1. If the number of electrons in the outermost shell is is equal to or less than
   3, say 2 electrons, then the valency will be 2.

2. If the number of electrons in the outermost shell is is equal to or more than
   4, say 6 electrons, then the valency will be {{< rawhtml >}}<math>8 - 6
   = 2.</math>{{< /rawhtml >}}

The latter suggests that atoms may share their electrons with other compatible
atoms such that both atoms can have 8 electrons in their outermost shell. And
that's completely correct! This is termed as 'combing capacity'.

**Therefore, an atom of each element has a definite combining capacity, called
its valency.**

### Covalent Bonds

Now introducing bonds might be a _bit_ too much for this one post but I will
keep it brief. A chemical bond is an association of atoms to form molecule,
compounds or other structures.

**The bond made from the sharing of electrons between atoms in their outermost
shell is called a covalent bond.** I think that this fact, that atoms share
electrons makes the concept of valency much more clear. And indeed this is why
elements and compounds react with each other, to make covalent bonds and reach
octet.

{{< figure align=center  src="/images/chemistry/atoms/covalent_bond_1.svg" alt="Diagram showing two chlorine atoms one electron away from an octet state." caption="Two chlorine atoms one electron away from an octet state" >}}

{{< figure align=center  src="/images/chemistry/atoms/covalent_bond_2.svg" alt="Two chlorine atoms sharing one electron each to maintain their octet state" caption="Two chlorine atoms sharing one electron each to maintain their octet state">}}
