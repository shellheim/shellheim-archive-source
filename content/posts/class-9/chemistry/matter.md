---
title: "What's Matter?"
date: 2023-09-10T17:52:32+05:30
draft: false
author: Shellheim
tags: ["chemistry", "science", "matter"]
---

**Any substance which occupies space and has mass is called matter. It makes up everything single thing in the universe.** It's the building block of everything in the universe.

The nature of matter is **particulate** which can be proven by various experiments. Such as the potassium permanganate({{< rawhtml >}} KMnO<sub>4</sub> {{< /rawhtml >}}) experiment.

In short, the experiment shows how you can colour a large amount of water using a tiny amount of potassium permanganate. This, consequently, proves that matter must be made up of very, very small particles and divides itself into smaller and smaller particles.

## Characteristics of Particles of Matter

### Particles of Matter are Very Very Small

The {{< rawhtml >}} KMnO<sub>4</sub> {{< /rawhtml >}} experiment above suggests that the particles which compose matter must be very, very small.

### Spaces Between Particles

If we mix salt or sugar in water and stir, we make a sugar/salt solution. Same if we dissolve lemon juice in water to make lemonade. We sweeten tea (and other beverages) with sugar and they dissolve into the mix. This shows that there must be space between particles of matter which these solutes occupy.

### Particles of Matter are Continuously Moving

If we put a drop of ink in a water filled beaker and leave it undisturbed then after a while it mixes into the water and the water becomes coloured. This phenomena is called **diffusion.**

Diffusion is the phenomena in which particles of a substance move from a region of high concentration to a region of low concentration.

![Diffusion](/images/chemistry/matter/matter/diffusion.webp)

Similarly, if we put spray perfume in a corner of a room it quickly spreads through the room. This indicates that the particles of the perfume **diffuse** through the air,i.e., it moves through the air.

Particles move constantly because they possess what's called kinetic energy (the energy of an object due its motion). This is related to their temperature. Higher temperature means more kinetic energy.

That's why if you leave two water filled beakers, one with a higher temperature than the other, mixed with say a drop of ink then the higher temperature beaker will have the ink diffuse first because some of the heat energy of the water will get transferred to the ink particles and both will move and collide more frequently. Resulting in higher rates of diffusion.

## States of Matter

Matter, on the basis of physical state, is divided into three categories :

```goat

      Matter
        |
.---+---.---+---.
|       |       |
Solid  Liquid  Gas
```

### Solid

**Substances which have fixed shape and volume and display characteristics of rigidity, negligible incompressibility, and of least intermolecular space between its constituent particles are called solids.**

Solids are generally the most strong form of matter and possess these characteristics:

- definite shape
- definite volume
- generally denser than liquids and gases

Solids are classified into two groups based on their arrangement of particles.

```goat

           Solids
             |
   .---------.---------.
   |                   |
Amorphous          Crystalline

```

#### Amorphous Solids

The solids which do not have a regular, orderly geometric atomic arrangement are called amorphous solids. These solids have a random arrangement of molecules. Because of such arrangements they have specific **properties.**

**Properties of Amorphous Solids**

- Due to random arrangement of the molecules of amorphous solids the strength between their bonds is also random. This makes them melt over a **broad range of temperatures.**

- They are **isotropic**, i.e. , they react the same to forces applied from any direction. Because they are arranged haphazardly they don't have directional variations. Think of it like a dough, same from every direction.

- They don't give a clean cleavage ;) It means that they don't break in a straight line with plane surfaces like crystalline solids. For example, crystalline solids will break like this :

{{< figure align=center src="/images/chemistry/matter/crystalline.svg" alt="Image depicting straight lines across atoms of a crystalline solid" caption="Forces can be exerted on a straight plane on crystalline solids" >}}

While amorphous solids will always have some atoms/molecules in the way of these forces resisting breakage and breaking in random shapes, like this :

{{< figure align=center src="/images/chemistry/matter/amorphous.svg" alt="Image depicting straight lines across atoms of a crystalline solid" caption="Forces can't be exerted on a straight plane on amorphous solids due their atomic structure" >}}

#### Crystalline Solids

The solids which have a regular, orderly geometric atomic arrangement are called amorphous solids. These solids have a fixed, predictable, geometric arrangement of molecules and/or atoms.

They are the opposite of amorphous solids. Thus, they have opposite **properties.**

- Since they have a regular arrangement of atoms/molecules, the strength of their bonds is also regular, i.e., same everywhere. Because of this they melt at a sharp temperature known as their melting point.

- They are anisotropic, i.e. , they react differently to forces exerted upon them from different directions.

- They give a clean cleavage, i.e, they break in straight lines with plane surfaces. Like shown above.

### Liquid

**A state of matter which has a fixed volume but no fixed shape are called liquids.**

#### Properties of Liquids

- **Variable Shape** .As outline in the definition, a liquid's constituent particles don't have enough intermolecular force between them to keep the liquid in a fixed shape. Thus, it lacks that.

- A liquid's **density** is generally higher than a gas and less than a solid (exception - water)

- **Diffusion**.The particles of two liquids placed near each other, enter the space between the other liquid's particles. They mix continuously until the mix of both the liquids is at an even concentration. This phenomena is called diffusion.

- **Negligible compressibility.** At 300K (27° C) when the pressure on a gas is doubled (from 1 atm to 2 atm) its volume is reduced by 50% but the same increase of pressure on a liquid only reduces its volume by about 0.0045%.

- **Evaporation.** Liquids may vaporize without reaching their boiling point, i.e., at room temperature.

- **Surface Tension**, this will be covered in the next section.

#### Surface Tension

The property of a liquid to reduce its surface area to the minimum is called surface tension.

This phenomena occurs due to **cohesive force.** The force responsible for the attraction between one type of particles is called the cohesion force. In water specifically, this cohesion is because of hydrogen bonds.

The molecules of {{< rawhtml >}} H<sub>2</sub>O {{< /rawhtml >}} at the top don't have any force exerted on them from the top unlike the molecules below them. This results in them pulling each other slightly closer and leads to the tendency of liquid to reduce it's surface area.

This is why an iron needle, while being more dense than water, can still float on it.

{{< figure align=center src="/images/chemistry/matter/floating_needle.webp" alt="Image showing an iron needle floating on water's surface" caption="An iron needle floating on water due to surface tension" >}}

**Surface tension is why raindrops are spherical.** Since a sphere has the least surface-area-to-volume ratio out of all solid shapes.

### Gas

The state of matter which has neither fixed volume nor fixed shape is called a gas.

This happens due to the large intermolecular spaces between the molecules of water which weaken the intermolecular space and thus make a shapeless substance with a **_non-fixed_** volume.

#### Properties of Gases

- **Compressibility.** Due to having large intermolecular spaces, gases are the most compressible form of matter.

- **Equal Pressure Exertion.** Solids exert pressure only downwards, liquids both downwards and to the side but gases exert pressure equally in all directions. This is because gaseous molecules have the most kinetic energy and, thus, move to strike the walls of their containers in all directions.

## Interconversion of States of Matter

{{< figure align=center src="/images/chemistry/matter/interconversion.svg" alt="An interconversion diagram" >}}

The states of matter can be interchanged into one another by using their two properties:

## Temperature

### Solid to Liquid & Vice Versa

1. Melting/Fusion

The process by which a solid transitions into a liquid by gaining heat energy is called melting/fusion.

The constant temperature at atmospheric pressure at which a solid melts into a liquid is called it's **melting point.**

**HOW :** When heat energy is supplied to a solid it's particles gain kinetic energy and start to vibrate more intensely. This energy overcomes the intermolecular forces keeping them together and at this point they start to melt into a liquid.

The amount of energy required to turn a solid into a liquid at it's melting point at atmospheric pressure is called **latent heat of fusion.**

{{< figure align=center src="/images/chemistry/matter/melting_icecubes.gif" alt="GIF of icubes melting" caption="Ice cubes melting" >}}

2. Freezing/Solidification

The process by which a liquid transitions into a solid by losing kinetic energy at atmospheric pressure is called **freezing.**

The constant temperature at atmospheric pressure at which a liquid freezes is called it's **freezing point.**

### Liquid to Gas & Vice Versa

1. Boiling/Vaporization

The process due to which a liquid changes into a gas and it's vapour pressure gets equal to the atmospheric pressure is called **boiling.**

The temperature at which a liquid starts to vaporize at atmospheric pressure is called it's **boiling point.**

To make a liquid transition into a gas it's temperature can be increased to it's boiling point.

**HOW :** When heat is supplied to a liquid, say water, it's particles gain kinetic energy. Water's particles obviously have intermolecular forces. When heat is supplied it increases the kinetic energy of water's particles and they get high enough kinetic energy (remember high kinetic energy means faster movement of particles.) to break off from the intermolecular forces (like hydrogen bonds) and at this point the liquid starts changing into a gas.

The amount of energy required to turn a liquid into a gas at it's boiling point at atmospheric pressure is called **latent heat of vaporization.**

2. Condensation

Condensation is a process directly opposite of boiling. **It's a process in which a gas transitions into a liquid by losing heat energy.**

The temperature at which a gas condenses into a liquid at atmospheric pressure by losing heat energy is called it's **condensation point.**

3. Sublimation

When a solid transitions directly to the gaseous state without first changing into a liquid, it's called **sublimation.** These substance also change back directly to solids when they lose heat energy without changing into a liquid first.

The substances which go through this process are called **sublimates.** Common substances which sublime are camphor, iodine, naphthalene, ammonium chloride etc.

## Pressure

The different states of matter exist due the difference between their intermolecular spaces. An increase of pressure on a gas decreases this intermolecular space and will liquefy the gas.

So, an increase in pressure and a decrease in temperature can liquefy a gas.

## Evaporation

When a liquid transitions into a gas without reaching it's boiling point at atmospheric pressure, it's called **evaporation.**

Basically, when the process of **vaporization** occurs below a liquid's boiling point it's called evaporation. It's **only a surface phenomena.** That's how it's different from vaporization/boiling because unlike vaporization it only occurs at the surface where particles have enough energy to overcome intermolecular forces and transition into a gaseous state.

### Factors Affecting Evaporation

These factors affect evaporation :

#### Temperature

An increase in temperature also increase the rate of evaporation.
Temperature is directly proportional to the **Rate of Evaporation.**

This is because more heat provides enough kinetic energy for the particles of the liquid to overcome their intermolecular forces and transition into a gaseous state faster.

#### Surface Area

Since evaporation is a surface phenomena, more surface area allows more particles to evaporate at the same time, increasing the rate of evaporation.  
So, surface area is directly proportional to the rate of evaporation.

#### Humidity

On evaporation the gas particles go into the air around them. If humidity (amount of water vapour in the air) is high then there will be less space for the evaporating particles, thus, lowering the rate of evaporation.

Hence, humidity is inversely proportional to the rate of evaporation.

#### Wind Speed

As stated above,on evaporation the gas particles go into the air around them. If wind speed is high, then the wind will take the recently evaporated particles away from the site of evaporation.That is, decrease humidity. This will increase the rate of evaporation.

Thus, wind speed is inversely proportional to the rate of evaporation.

### Cooling Caused By Evaporation

A liquid evaporates due to the absorption of heat energy from a source. When a liquid's particles evaporate they absorb the surrounding heat energy and make the area feel cooler.

This is why if we put a drop of a liquid with a low boiling point, like acetone, on our hand we feel cooler there when it evaporates.

{{< figure align=center src="/images/chemistry/matter/acetone.webp" alt="Image of Acetone" caption="" >}}
