---
title: "Important Dates of the French Revolution"
date: 2023-09-09T19:30:41+05:30
draft: false
author: Shellheim
tags:
  [
    "History Dates",
    "Dates",
    "Cheatsheet",
    "France",
    "Revolution",
    "History",
    "French Revolution",
  ]
---

Hello! I am back after a whole damm month! If you remember, I had an exam which didn't allow me to publish. Coming weeks should be fine with a focus on science but also a little bit of the Russian Revolution.

This post is a table of important dates of the french revolution.

| Date          | Headline                            | Event                                                                                                                                                                                                                                   |
| ------------- | ----------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 5 May, 1789   | Call of the Estates Generals        | King Louis XIV called a meeting of the estates to discuss raising taxes on the third estate                                                                                                                                             |
| 17 Jun, 1789  | Tennis Court Oath                   | The thrid estate representatives form the national assembly                                                                                                                                                                             |
| 20 Jun, 1789  | Tennis Court Oath                   | The national assembly took an oath not to separate and reassemble until the constitution of the kingdom is established.                                                                                                                 |
| 14 Jul, 1789  | Storming of the Bastille            | The fort Bastille functioning as a prison in Paris was stormed and destroyed.                                                                                                                                                           |
| 4 Aug, 1789   | Abolition of Fedudal Privileges     | The national assembly passed decree abolishing the privileges of the nobility and the clergy                                                                                                                                            |
| 5-6 Oct, 1790 | Women's March on Versailles         | Jean-Paul Marat's newspaper called on the people to march to Versailles and protest the King's suppossed disrespect to the tricolour (this was incorrect). Thousands of women marched to Versailles and brought the King back to Paris. |
| 20 Jun, 1791  | King's Unsuccessful Escape          | The King of France Louis XIV and his wife and Queen Marie Antoinette with their children tried to escape from Paris and failed.                                                                                                         |
| Jan 21, 1792  | Execution of the King and the Queen | The King and Queen are found guilty of treason and executed by the guillotine.                                                                                                                                                          |
| May 31, 1793  | Overthrow of the Moderates          | The National Convention was surrounded by Sans-Culottes and the moderates politicians were arrested. Robespierre was in total control.                                                                                                  |
| 28 Jul, 1794  | Execution of Maximilien Robespierre | The National Convention when Robespierre threatened by Robespierre that many of its memebers were to be sent to the guillotine decided to execute Robespierre.                                                                          |
