---
title: 'The French Revolution'
date: 2023-07-28T16:09:16+05:30
draft: false
author: Shellheim
tags: ['France', 'Revolution', 'History', 'Democracy', 'French Revolution']
---

The French Revolution took place in 1789. It came with it's own promises of
equality, liberty, fraternity etc. It failed on many and became the starting
point for some of them and lead to their realization in the modern world today.

These are pretty short summaries of the causes, happening and the aftermath of
the revolution.

## The Old Regime : The Feudal French Society

The French society was three tiered:

{{< figure align=center src="/images/history/the-french-revolution/french-society.webp" alt="Feudal French Society Structure" caption="Courtesy : [Oversimplified](https://www.youtube.com/watch?v=8qRZcXIODNU)" >}}

**1. The First Estate : The Clergy**

At the top was clergy - men of the church (e.g. Bishops, Pastors etc.) - as they
were considered holy men.

**2. The Second Estate : The Nobility**

The second were the nobility, royals or related to royals. The King Louis XVI
did come into the second estate but he and his close relatives were above the
clergy for obvious reasons.

**3. The Third Estate : Peasants, Lawyers, Administration Officers etc.**

The last were the common people - the peasants.

The first two tiers,the clergy and the nobility, had to pay little to no taxes
and the poor working class had to carry the burden of the rich and powerful.

Major Taxes Included :

-   Income Tax
-   Tithe (given to the church, 1/{{< rawhtml >}}10<sup>th</sup>{{< /rawhtml >}}
    of agricultural produce.)
-   Labour Tax (peasants had to work for their local lord for a certain number
    of days without payment.)
-   Feudal Dues (Extracted by nobles, often violently.)

and many more indirect taxes.  
This taxation scheme was also very inconsistent around France.

## The Emerging Middle Class : Top Brass of the Third Estate

The wages of peasants were fixed but the rise in the french population from 23
million in 1715 to 28 million in 1789 meant more demand for food which caused
the prices of bread and other necessities to rise. Not being able to afford food
created a subsistence crisis for the peasants.

This wasn't new and previous revolts had taken place because of this but they
didn't have the means to communicate and change society effectively.

This was the advantage of the new middle class as compared to their historically
poorer ancestors. They emerged as a new class in the
{{< rawhtml >}}18<sup>th</sup>{{< /rawhtml >}} century by means of expanded sea
trade and textiles sales. But more importantly they consisted of lawyers and
administration officials who were educated and had the resources to organize new
revolts and bring effective change.

**Publications that effected the revolution :**

| Book Title                  | Writer                 | Year of Publication |
| --------------------------- | ---------------------- | ------------------- |
| The Social Contract         | Jean-Jacques Rousseau  | 1762                |
| Two Treatises of Government | John Locke             | 1689                |
| The Spirit of the Laws      | Montesquieu            | 1748                |
| What is the Third Estate?   | Emmanuel Joseph Sieyès | 1789                |
| The Rights of Man           | Thomas Paine           | 1791                |

In his `Two Treatises of Government` John Locke challenged the concept of divine
right to rule and the absolute power of the monarch.

Montesquieu's `The Spirit of the Laws` he gave the idea to divide power into
between legislative, the executive and the judiciary.

Jean-Jacques Rousseau proposed a `social contract` between the governed and
their representatives that as llong as their needs are met they shall keep a
civil and peaceful society in place.

{{< figure  src="/images/history/the-french-revolution/rousseau.webp" alt="Portrait of Jean-Jacques Rousseau by Maurice Quentin de La Tour" caption="Jean-Jacques Rousseau by [Maurice Quentin de La Tour](https://en.wikipedia.org/wiki/Maurice_Quentin_de_La_Tour)" >}}

> Note : Montesquieu's The Spirit Of the Laws also inspired USA's constitution
> for division of power.

## The Outbreak : Tipping Point for the Peasants

The subsistence crisis was worsened by a series of natural disasters which
affected the food supply. However, the nobility were virtually untouched by this
crisis since they had enough food in stock. Unable to afford bread women started
raiding shops and setting fire to them, sometimes shop owners suspected of
hoarding bread were killed.

To combat the rising debt on France (of 2 billion+ livres) Louis XVI called the
**`Estates General.`**

The Estates General was a purely advisory body to the king and hadn't been
called since 1614 (175 years ago.) On
{{< rawhtml >}}5<sup>th</sup>{{< /rawhtml >}} May 1789, the king called the
Estates General to discuss new tax laws.

The Estates General meeting comprised of the first, second and the third estate.
Each getting one vote. Right at the start the discussion, discrimination could
be seen. The first and second estate representatives (300 each) sat facing each
other while the 600 third estate representatives had to stand at the back.

Traditionally voting had been done by the principle that each estate had one
vote, but the third estate asked the king that a representative system be set up
so they could have a fair participation in the process. On being denied this
they walked out and went inside a nearby tennis court and formed the
**`national assembly`** and took the **`tennis court oath`**.

{{< figure  src="/images/history/the-french-revolution/tennis_court_oath.webp" alt="The Tennis Court Oath Painting by Jacques-Louis David" caption="The Tennis Court Oath by [Jacques-Louis David](https://en.wikipedia.org/wiki/Jacques-Louis_David)" >}}

They were lead by Mirabeau (originally a noble, believed feudal privileges
should be abolished. ) and Abbé Sieyès (a priest and thus a member of clergy.)

Louis XVI decided to call in the army to protect himself from what he saw as the
dangerous peasants. This only added to the peasants' fear and finally this fear
on {{< rawhtml >}}14<sup>th</sup>{{< /rawhtml >}} July, 1789 resulted in the
destruction of the prison fortress of Paris, and a symbol of royal tyranny,
`The Bastille.`

{{< figure  src="/images/history/the-french-revolution/bastille.webp" alt="The Storming of the Bastille" caption="The Storming of the Bastille" >}}

In the October of 1789, a group of women 7000 strong marched to Versailles, with
more people joining along the way and raided the palace of the king. The mob
killed several guards and brought the king back to Paris with them. The King
finally agreed to share power with the new revolutionary Government.

## Changes in French Politics : A New Republic

On the night of {{< rawhtml >}}4<sup>th</sup>{{< /rawhtml >}} August 1789, the
National Assembly passed laws abolishing the feudal system, it's taxes and
privileges. The new government confiscated lands owned by the church and
abolished tithes. This resulted in the government acquiring assets worth 2
billion livres.

The Assembly drafted the french constitution of 1791 and introduced elections.

Key Notes of the Constitution :

-   Voting rights for men above 25 years of age whose tax equated to at least
    three days of a labour's wage.
-   Qualified voters were called `active citizens.`
-   No voting rights for women.
-   Unqualified voters were called `passive citizens.`
-   Only beneficial to the upper middle class.

The constitution despite it's shortcomings inspired many other revolutions and
movements for freedom, liberty and democracy.

The constitution began with **`Rights of Man.`**

> Note : Thomas Jefferson also helped in the formation of rights of man.

The Rights of Man was a set of rudimentary fundamental rights.

**Summary of The Rights of Man.**

-   Men are born and remain free and equal in rights.

-   The aim of every political association is the preservation of the natural
    and inalienable rights of man; these are liberty, property, security and
    resistance to oppression.

-   Sovereignty resides in the nation. No body or individual may exercise power
    that does not come from the people.

-   Liberty is the power to do whatever is not injurious to others.

-   The law has the right to forbid only actions that are injurious to society.

-   Law is the expression of the people. All Citizens have the right to
    participate in it's formation. All are equal before it.

-   No man may be accused, arrested or detained, except in cases determined by
    the law.

-   Freedom of speech and expression, the citizen must take responsibility of
    such liberty in cases determined by law.

-   For the maintenance of society a common tax is levied on everyone in
    proportion to their means.

The king had agreed to share power but secretly was in talks with the king of
Prussia and other neighbouring countries to restore his power. Before they could
wage war the assembly voted in April 1792 to declare war against Prussia and
Austria.

## The Jacobin Club : The Lower Class Revolts

{{< figure  src="/images/history/the-french-revolution/robespierre.webp" alt="Maximilian Robespierre's Portrait" caption="Maximilian Robespierre" >}}

The lower-class who were deprived of rights even under the constitutional
monarchy formed their own groups. One of the most popular among them was the
Jacobin Club. Lead by Maximilian Robespierre, it was a radical group looking to
depose the king entirely (as opposed to moderates, who wanted to keep him as a
figurehead.)

> Note : The Name 'Jacobin' came from the St. Jacob Convent in Paris.

He advocated for rights of the lower class people and was a revolutionary
extremist. Large sections of Jacobins started wearing striped trousers to set
themselves apart from the fashionable nobility. Especially, the knee-breech
wearing aristocrats, they were known as the **`Sans-Culottes.`**

{{< figure  src="/images/history/the-french-revolution/sans-culottes.webp" alt="A Sans Culottes Man Holding the Movement's Flag" caption="A Romanticized Sans Culottes Man by [Louis-Léopold Boilly](https://en.wikipedia.org/wiki/Louis-L%C3%A9opold_Boilly)" >}}

When King Louis and Queen Marie Antoinette tried to escape France on June
{{< rawhtml >}}20<sup>th</sup>{{< /rawhtml >}}1792 and were caught, the Jacobins
(radicals) wanted to depose the King entirely. Simplifying a bit, the French
revolutionaries feared the intervention of their neighbouring countries to end
the revolution and declared war on Austria and it's allies joined in too.

On {{< rawhtml >}}10<sup>th</sup>{{< /rawhtml >}} August 1792, the angry public
marched into King Louis' Palace and tried to kill him. He sought refuge in the
national assembly. Unfortunately for him the radical factions had gained greater
power there and voted to completely remove the King and the Queen and imprison
them.

New elections were held and now men above 21 years of age could vote. The newly
elected government was called the `National Convention.` On 21 September 1792
France was declared a republic.

King Louis was charged with treason and executed on
{{< rawhtml >}}21<sup>st</sup>{{< /rawhtml >}} January 1793. Queen Antoinette
met the same fate shortly after.

## The Reign of Terror : Bloodshed in the Name of Liberty

After the elections of 1792, Robespierre had more control than ever. The period
from 1793 to 1794 is referred to as **`The Reign of Terror.`**  
Robespierre exercised severe control over France, akin to a dictator.

Some of his policies were :

-   rationing bread and meat
-   a price cap on grains being sold
-   a price cap on wages and prices to control inflation
-   banning the use of expensive bread and forcing everyone to eat
    `equality bread` made of wholewheat
-   banning the words `Monsieur` (Sir) and `Madame` (Madam) and introducing
    instead, `Citoyen` and `Citoyenne`

His extremism backfired, his own supporters turned against him and he was
finally executed in July 1794.

## The Directory : Failures and Napoleon

After the fall of Robespierre and his government the upper middle class seized
power again. They reverted the rules like men being able to vote irrespective of
wealth. This government introduced a new constitution which made two legislative
councils, then these councils elected a _directory._

The directory, which was an executive, consisted of 5 members so as to avoid one
man seizing all power as in the case of Robespierre. But the directory often saw
infighting between its directors. Due to being in this state it could not
introduce economic stability to France which helped a certain Napoleon Bonaparte
to rise to power.

## A Short Introduction : Olympe de Gouges

The following text is taken as is from the NCERT textbook for History (Class 9)
:

{{< figure src="/images/history/the-french-revolution/Olympe_de_Gouges.webp" alt="Olympe de Gouges Portrait" caption="Olympe de Gouges by [Alexander Kucharsky](https://en.wikipedia.org/wiki/Alexander_Kucharsky)" >}}

**The life of a revolutionary woman – Olympe de Gouges (1748-1793)**

Olympe de Gouges was one of the most important of the politically active women
in revolutionary France. She protested against the Constitution and the
Declaration of Rights of Man and Citizen as they excluded women from basic
rights that each human being was entitled to.

So, in 1791, she wrote a Declaration of the Rights of Woman and Citizen, which
she addressed to the Queen and to the members of the National Assembly,
demanding that they act upon it. In 1793, Olympe de Gouges criticised the
Jacobin government for forcibly closing down women’s clubs. She was tried by the
National Convention, which charged her with treason. Soon after this she was
executed.

## Conclusion

The revolution failed many of it's promises like true equality, freedom of
speech and expression etc. but it did also become a foundation for the
propagation of liberty and modern democracy. It inspired whole generations of
revolutionaries,both in Europe and outside in the world, to fight oppression and
became a precedent for the people's unity.
