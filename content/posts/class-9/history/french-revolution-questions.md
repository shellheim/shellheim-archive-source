---
title: 'French Revolution Questions'
date: 2023-08-09T10:26:48+05:30
draft: false
author: Shellheim
tags:
    [
        'Questions',
        'Test',
        'NCERT',
        'Cheatsheet',
        'France',
        'Revolution',
        'History',
        'French Revolution',
    ]
---

These are the questions from the
[NCERT textbook](https://ncert.nic.in/textbook.php?iess3=0-5) of History for
class 9th, Chapter 1 : The French Revolution.

NOTE : Please keep in mind the fact that these are not definite, these answers
are solely my point of view which I think can be said for the historical
scenarios, others can form different conclusions (given they are supported by
reason and evidence).

I hate to see it in India how students are forced to regurgitate the book to
focus on marks instead of the reasons of history.

## NCERT Questions

### 1. Describe the circumstances leading to the outbreak of revolutionary protest in France.

Ans : Since Louis XIV became king France faced a huge economical problem of a
debt of 2 billion livre. It also faced huge costs of maintenance of the army,
it's royal courts, the king's extravagant court and his palace.

To meet these costs Louis XIV called a meeting of the estates-general to discuss
an increase of taxes. This was seen as very unfair since the meeting comprised
of all the three estates only two of which had to pay taxes and all had one vote
irrespective of their population in the country. This meant any proposal of
increase in taxes would pass by a majority of 2 votes, since the first and
second estates weren't affected by the change. This caused the third estate's
representatives to revolted and make their own assembly called
`the national assembly.` Having to pay higher taxes made it harder to afford
food for the third estate.

The dramatic rise of population by 5 million between 1715 and 1789 meant more
demand for food but supply couldn't keep pace with demand and resulted in an
increase in food prices. All these factors where enough for small outbursts but
the tipping point came after France went through a harsh winter which reduced
the food supply even more. Seeing the outbursts the king called in battalions to
surround Paris.

Fear settled in the peasants that the king was about to order the execution of
all the protesters. All of these factors combined finally came out in the form
of the Revolution.

### 2. Which groups of French society benefited from the revolution? Which groups were forced to relinquish power? Which sections of society would have been disappointed with the outcome of the revolution?

Ans : The top brass of the third estate like lawyers, doctors, wealthy
businessmen, administration officials, etc. benefited from the revolution since
it put more power into their hands and gave them more rights for paying taxes
which previously yielded no benefits.

The nobility and clergy were forced to relinquish their social status, executive
powers and their feudal privileges like tax exemption.

The poorest and most powerless sections of french society, i.e, the common
peasants, landless labourers, women etc. were disappointed with the initial
outcome of the revolution.

### 3. Describe the legacy of the French Revolution for the peoples of the world during the nineteenth and twentieth centuries.

Ans :

The French Revolution served as great a inspiration against tyranny and
feudalism in the early to mid nineteenth centuries.

-   It became a birthplace for democratic ideals like equality, fraternity,
    right to vote etc. and many other democratic systems and institutions.

-   It inspired future revolutions both in France and elsewhere in Europe,
    against colonialism in many African countries, central-asia, southern-asia
    etc.

-   It left a lasting impact, not only in Europe, but the world in giving the
    oppressed sections of those regions new ideals of sovereignty and democracy.

-   Most importantly it challenged the long held belief of divine right to rule.

### 4. Draw up a list of democratic rights we enjoy today whose origins could be traced to the French Revolution.

Ans : These rights may be traced to the French Revolution :

-   Equality of Citizens irrespective of caste, creed, social status etc.
-   Sovereignty of Individuals and Countries
-   Freedom of Expression
-   Freedom of Religious Expression
-   Universal Adult Franchise (even though it was absent in the original
    revolution of 1789)

### 5. Would you agree with the view that the message of universal rights was beset with contradictions? Explain.

Ans : I agree that the message of universal rights was quite hypocritical in
nature. It contradicted itself on quite a few points :

-   The rights of sovereignty proclaimed that no person or authority may
    exercise power that doesn't come form the people. However, by `the people`
    it means only men who can pay taxes equal to three days of wages of a
    labourer. Which neglected 20 million men and women and reduced them to
    passive citizens. It forbade any political rights to the majority of
    `the people.`

-   Even the male passive citizens enjoyed most basic rights (except the right
    to vote) like right to property, sovereignty, freedom of expression, being
    able to start, lead, and participate in political clubs. However, women's
    rights were left untouched and their life barely changed in this aspect even
    after the first revolution and second revolution.

-   The oppression of women was supported by arguments like nature had given
    women the resources to do domestic duties only. The argument of nature's
    assignment was also used by monarchs before as `the divine right to rule.`

### 6. How would you explain the rise of Napoleon?

Ans : After the fall of Robespierre the directory struggled to rule France. It's
directors often clashed internally which reduced the country's political will
and introduced economical instablity and thus didn't solve the basic problems
which were also presented in the old regime like subsistence crisis.

Napoleon took advantage of the political instablity of the directory and of the
public's discontent with them. He forcefully took control of the directory and
declared himself the ruler and eventually emperor. He faced little to no
resistance even after reverting the big changes of the revolutionbecause he
introduced economical stablity and the masses could live a more stable life.
