---
title: "Population"
date: 2023-12-14T21:37:36+05:30
draft: false
---

Long time no see. Trying to rush the leftover syllabus in physics, chemistry and maths. This is going to be a very short one just skimming the already small chapter 5 of Geography.

## The Data

According to the [2011 census](https://en.wikipedia.org/wiki/2011_census_of_India), India had **1.21 billion** people (now it has 14. billion which is the largest in the world) living in the country. While U.P. contributed a massive 16.5% (199 million, 241 million now) to the total Indian population.

{{< population-chart-share >}}

Even though Uttar Pradesh is the most populous state, due to it's sheer size it isn't the most crowded. Bihar is the most densely populated state.

| State         | Density (Per Sq. Km) |
| ------------- | -------------------- |
| Bihar         | 1001+                |
| West Bengal   | 1001+                |
| Uttar Pradesh | 751 - 1000           |
| Tamil Nadu    | 501 - 700            |
| Kerela        | 751 - 1000           |

## Causes of Non-uniform Population Densities

Listed above were the states with the most population density. Notice how all of these states are mostly **plains with fertile soil perfect for farmlands**. This make them **very suitable for living**, thus, **increasing their population densities**. Similarly, states with **rugged, mountainous and marshy landscapes** have **lower population densities** like Ladakh, Kashmir, Mizoram, Assam etc.

## Population Growth as a Phenomena

Population growth is quite dynamic and multi-faceted. It depends on multiple factors like economy, financial conditions of a family, religious beliefs and culture.

Population growth may be defined as **the increase in the number of inhabitants of country/region during a period of time.**

It may be seen as either the absolute increase in numbers (+3 million) or as a percentage (+10%). Huge countries (like India) have giant increases every decade, for example, India added 1.3 crore people in a year (2023 - 2024) which, on its own, looks like a huge increase. But percentage wise it was only a +0.92% change.

This chart shows how population rates have come down over the years.

{{< population-chart-growth-rate >}}

Yet, the overall population has kept increasing.

{{< population-chart-numbers >}}

**It's clear that when a low annual rate is applied to a very large population, it yields a large absolute increase.**

This is why birth rates are still crucial because even though they're low, the numbers they yield can affect resources available in the country.

## Change of Populations

**1. Birth Rate** is the rate of births per 1000 people each year.

**2. Death Rate** is the rate of deaths per 1000 people each year.

**3. Migration** is the movement of people from one region to another for permanent residency.

These factors affect how the population of a region changes. 1 & 2 change the numbers of the population and 3 affects its distribution. One reason for the explosive growth of the Indian population is **declining death rates due to better access to medical care.** Simultaneously, better healthcare also reduces premature death of infants contributing to the increase. This first started happening substantially in the 1980s.

The third reason (Migration) which may be either inside the country or outside it. This only changes the distribution of population. Like how the urban population went to 36% in 2024 from 18% in 1951.

Then there's propaganda about the government actually doing something for kids with malnutrition, which I am going to skip.
