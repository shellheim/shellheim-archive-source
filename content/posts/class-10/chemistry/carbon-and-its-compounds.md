---
title: 'Carbon and Its Compounds'
date: 2024-12-28T15:09:51+05:30
draft: false
---

## Covalent Bonds

As I have [written before](/posts/class-9/chemistry/atoms/#covalent-bonds),
**the bond made from the sharing of electrons between atoms in their outermost
shell is called a covalent bond.**

{{< figure align=center  src="/images/chemistry/atoms/covalent_bond_2.svg" alt="Two chlorine atoms sharing one electron each to maintain their octet state" caption="Two chlorine atoms sharing one electron each to maintain their octet state" >}}

### Properties of Covalently Bonded Molecules

There are some properties associated with covalently bonded molecules:

1. **Strong bonds** between molecules

2. **Weak intermolecular forces.** Due to which they also have:

    - Low melting point and boiling point

3. Covalent bonding **does not result in the formation of ions**, thus they are
   **not good conductors of electricity.**

## Allotropes

**When an element can can have several structural forms while being in the same
physical state, is called allotropy.**

And **these forms are called allotrope of the element.** There are two types of
carbon allotropes:

**1. Crystalline**

**2. Amorphous.**

{{< figure src="/images/chemistry/carbon-and-its-compounds/allotropes.svg"
caption="Allotropes of carbon" alt="Flowchart of allotropes of carbon" >}}

## Crystalline Form

### Diamond

- Physical properties

    - Hardest natural substance known
    - Colorless and transparent
    - Very bright shine
    - High-meriting point (About 3500°C)
    - Non-conductor of electricity

- Chemical properties.

    - When diamond is burnt in the presence of oxygen, only carbon dioxide is
      formed, with nothing else left behind, thus diamond is only made up of
      carbon.

    - Therefore, its symbol is also the same as carbon, i.e, 'C'.

- Structure

{{< figure
src="/images/chemistry/carbon-and-its-compounds/carbon_lattice_diamond.svg"
caption="Structure of Diamond" alt="Diamond Cubic" >}}

- Each carbon atom is bonded to 4 carbon atoms in a 3 dimensional rigid
  structure.

- There are 4 carbon atoms at the vertices of a regular tetrahedron.

- It doesn't conduct electricity because all 4 **valence electrons** of carbon
  atoms **are used in covalent bonds** with 4 other carbon atoms, **leaving no
  free electrons.**

- Uses

    - Cutting glass and drilling rocks
    - Jewellery making because of its great ability to reflect and refract light
    - used by eye surgeons as a tool to remove cataract.

### Graphite

1.  Physical properties

    - Graphite is a greyish black substance.
    - It's smooth and slippery.
    - It's a good conductor of electricity.
    - It has a high melting.

2.  Chemical properties.

    - If burnt in the presence of oxygen, it only leaves carbon dioxide behind.
      Therefore, it can be assumed that graphite is made up of only carbon. Its
      symbol is C.

3.  Structure

{{< figure src="/images/chemistry/carbon-and-its-compounds/graphite.svg" caption="Structure of Graphite"  alt="Graph showing the molecular structure of graphite." >}}

- Each carbon atom is bonded to 3 other carbon atoms and they form hexagonal
  rings.

- These hexagonal rings exist in multiple layers, over each other.

- Each **hexagonal ring layer is held together by weak Van der Waals forces.**

- Thus, **each layer can easily slip over each other**, making graphite **soft,
  and good for lubrication.**

- It's a **good conductor of electricity** because each carbon atom is only
  bonded to 3 other carbon atoms leaving one electron free.

### Buckminsterfullerene

- Named after American architect Buckminster Fuller, because he invented the
  geodesic dome which is similar to the structure of
  {{< rawhtml >}}C<sub>60</sub>{{< /rawhtml >}}.

- One molecule consists of 20 hexagonal and 12 pentagonal rings of carbon atoms.

- Used as lubricant, semi-conductor, superconductor.

{{< figure src="/images/chemistry/carbon-and-its-compounds/buckminsterfullerene.svg" caption="Structure of buckminsterfullerene" alt="Buckminsterfullerene molecular structure" >}}

## Catenation

- **The unique property of carbon to form bonds with other carbon atoms
  resulting in long chains of such molecules is called catenation.** The extent
  to which carbon can perform catenation is unseen in other elements.

- **Silicon can form compounds with hydrogen or oxygen for upto 7 or 8 atoms**,
  **but** these are very reactive and **they** therefore **are not useful.**

- On the other hand, **carbons can form very strong, long, and stable bonds.**
  This gives us many long compounds.

{{< figure src="/images/chemistry/carbon-and-its-compounds/branched_chain.svg" caption="Chains of Carbon" alt="Diagram showing linear, branched and ring chains of carbon" >}}

## Tetravalency

It's pretty simple, **'tetra' means 4**, thereofre `tetra` + `valency` =
`Tetravalency`.

- Carbon is **capable of bonding with 4 other atoms** of carbon or some other
  mono valent element, like hydrogen.

- Carbon bonds are **exceptionally stable because** carbon **atoms are** very
  **small.**

### Bonds & Structure

**1. Bonds**

**There are three kinds of chemical bonds**, namely:

- **Single Bond**, i.e., the sharing of **1 pair of electrons.**

- **Double Bond**, i.e., the sharing of **2 pair of electrons.**

- **Triple Bond**, i.e., the sharing of **3 pair of electrons.**

There are three types of bonds:

To make a simple compound, say,{{< rawhtml >}}CH<sub>4</sub>{{< /rawhtml >}}, we
have to **make sure a carbon atom has single bonds with 4 other hydrogen
atoms.**

Like this:
{{< figure align=center src="/images/chemistry/carbon-and-its-compounds/ch4.svg" alt="Atomic structure of Methan and Propane" caption="Atomic Structure of Methane and Propane" >}}

In the picture above, carbon has a **single bond with each hydrogen atom**,
i.e., it shares one electron with each hydrogen atom.

### Isomerism

**Compounds having the same molecular formula with different structure are
called structural isomers**. This phenomena is known as isomerism. Carbon
compounds are especially good at forming isomers.

Since the same molecular formula can result in different types of compounds,
there is a system of nomenclature designed to name carbon compounds. We'll look
into that later.

{{< figure align=center src="/images/chemistry/carbon-and-its-compounds/isomers.svg" alt="Diagram showing isomerism" caption="Isomers of {{< rawhtml >}}C<sub>3</sub>H<sub>8</sub>O{{< /rawhtml >}}" >}}

## Saturated & Unsaturated Compounds

Carbon compounds which only contain carbon and hydrogen are called
**hydrocarbons.**

### Saturated Compounds

**Compounds in which there are only single bonds between carbon atoms** are
called saturated compounds.

- These are **also called alkanes (-).**

For example, Methane.

### Unsaturated Compounds

**Compounds in which there are multiple (double or triple) bonds between carbon
atoms** are called unsaturated compounds.

- These are **also called alkene (=) and alkynes (≡).**

For example, Propylene or Propene.

### General Formulae of Hydrocarbons

**1. Alkane**

General Formula:
**{{< rawhtml >}}C<sub>n</sub>H<sub>2n+2</sub>{{< /rawhtml >}}.**

**2. Alkene**

General Formula: **{{< rawhtml >}}C<sub>n</sub>H<sub>2n</sub>{{< /rawhtml >}}.**

**3. Alkyne**

General Formula:
**{{< rawhtml >}}C<sub>n</sub>H<sub>2n-2</sub>{{< /rawhtml >}}.**

## Functional Groups

**A group of atoms in a hydrocarbon which imparts specific chemical
properties**, is called a functional group.

It's called so **because it changes the _functionality_** of the compound.

Functional groups are a group **heteroatoms**, i.e., any atoms other than carbon
or hydrogen in a hydrocarbon.

## Homologous Series

Homologous **series is a series of hydrocarbon compounds with similar chemical
properties and some functional groups, differing from the successive member of
the series by** one methylene group, i.e,
**{{< rawhtml >}}CH<sub>2</sub>{{< /rawhtml >}}.**

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/homologous_series.svg" alt="Diagram showing different compounds differing by one Methyl group." caption="Homologous Series" >}}

It should be noted that in the diagram above, the **physical properties of the
elements change because** the **size of carbon chain changes**. However, the
**chemical properties remain** the **same since** the **functional group remains
the same.**

## Nomenclature

As we know, isomerism can make it so the same molecular formula results in
different kinds of susbtances. Therefore, we need a system to name these
substances that gives us an idea about them.

These **guidelines were framed** and are maintained **by** International Union
of Pure and Applied Chemistry **(IUPAC)**

First, let's look at the table of stuff we are going to need.

| Substitue Prefix | Root\*\* Word | Type of Bond | Suffix    | F. Group Suffix |
| ---------------- | ------------- | ------------ | --------- | --------------- |
| - CH3\* (Methyl) | Meth-         | -ane (-)     | 2 (Di)    | -ol (-OH)       |
| - C2H5\* (Ethyl) | Eth-          | -ene (=)     | 3 (Tri)   | -al             |
| - Cl (Chloro)    | Prop-         | -yne (≡)     | 4 (Tetra) |                 |
| - Br (Bromo)     | But-          |              |           |                 |
|                  | Pent-         |              |           |                 |
|                  | Hex-          |              |           |                 |
|                  | Hept-         |              |           |                 |
|                  | Oct-          |              |           |                 |
|                  | Non-          |              |           |                 |
|                  | Dec-          |              |           |                 |

\* These are alkyl groups, i.e, a group derived from alkanes by removal of a
hydrogen atom from any carbon atom.

\*\* This is an indicator of the **number of carbon atoms in the main chain**,
of the compound. From Meth (1) to Dec (10)

### Writing Formula

Position of Substitute + Name of Substitute + Wordroot + Position of Double-Bond
or Triple-Bond + ane/ene/yne + Position of functional group + Suffix of
functional group.

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/formula.svg" alt="Formula for Writing the Names of Carbon Compounds" caption="Formula for Writing the Names of Carbon Compounds" >}}

### Rules of Nomenclature

**1. Longest Chain Rule**

First of all, the **longest carbon chain is identified.** There is a **priority
system in numbering chains.**

{{< figure src="/images/chemistry/carbon-and-its-compounds/carbon_chain.svg" alt="Two carbon chains being shown, each can be selected since they both have equal number (5) of carbon atoms." caption="Both Carbon Chains Can Be Selected." >}}

**2. Lowest Numebr Rule**

Number the longest chain such that, the **branched carbon atoms / substituents
get the lowest possible number.**

{{< figure src="/images/chemistry/carbon-and-its-compounds/branched_chain_numbering.svg" alt="Branched carbon chain being numbered such that the substituents (brached chain) gets lowest possible number." caption="Substituents should get lowest number possible." >}}

- If the chain has a **double or triple bond**, then the the carbon atoms
  involved in this bond **should get priority over branches** and should be
  given the lowest number possible.

{{< figure src="/images/chemistry/carbon-and-its-compounds/multiple_bond_numbering.svg" alt="Double bond containing chain being numbered such that the atoms involved in the bond get the lowest possible number possible." caption="Bonds should get lowest number possible, **prioritized over substituents.** " >}}

- If there is a **functional group in present** in the carbon chain, then the
  longest chain **_containing the functional group_** should be numbered in a
  such a way that they get the lowest number possible.

{{< figure src="/images/chemistry/carbon-and-its-compounds/functional_group_numbering.svg" alt="Chain containing functional group being numbered such that the functional group gets the lowest possible number possible." caption="Longest chain containing functional groups should get lowest number possible, **prioritized over substituents _and multiple bonds_.** " >}}

Thus, here we have a priority list:

**Functional Group > Double/Triple Bond > Branched Carbon Atoms / Substituents**

**3. Use of Prefixes**

If the longest chain **contains more than one substituent of the _same kind_**
then their positions, after numbering, are written separately and a prefix
according to their number is attached to that substituent's name, such as di,
tri etc.

Say you have 2 Methyl groups at 2 and one at 3, like here:

{{< figure src="/images/chemistry/carbon-and-its-compounds/multiple_substituents_numbering.svg" alt="Compound containing three methyl groups at different positions." caption="" >}}

Now, how are we supposed to name this? Let's go back to the
[writing system](#writing-formula) I mentioned previously. First, the positions
of the substituents is 2,2,3. Yes, if the same (or not) and **more than one
substituents** are **present at the same spot**, they both **get a number each
like 2,2.**

Now, the name of all the substituents is Methyl. The word root will be 'Pent'
since the main chain has 5 carbon atoms. **There are no functional groups or
multiple bonds here**, so we can skip things related to them. Now **we add 'ane'
suffix since all the carbon atoms have a single bond.** Thus, we have the name:

2,2,3-**Tri**methylpentane

That 'Tri' is because we have 3 of the same thing (Methyl group in this case).
Similarly, it could have been 'Di' if we had 2 Methyl groups only and so on.

> **Note:** Numbers are separated by commas (,) and letters and number are
> separated by dashes (-).

**4. Arrange Prefixes Alphabetically**

The **substituents prefixes** such as 'Methyl' and 'Propyl' **must come in
alphabetical order, irrespective of the numerical value of their positions.**

For example, if we have a compound with a **methyl group at 2** and an **ethyl
group at 3** such as here:

{{< figure src="/images/chemistry/carbon-and-its-compounds/susbtituent_prefix.svg" alt="Compound containing methyl at 2 and ethyl at 3" >}}

The name will be **3-Ethyl-2-Methylpentane** and NOT **2-Methyl-3-Ethylpentane**
even though 2 is less than 3 and you might think, should come first.

**5. Same Positions Rule**

Give the **lowest number to the alphabetically first substituent** if more than
one of them is present and left to right **and** right to left **numbering yield
the same array of positions.**

Again, here's an example:

{{< figure src="/images/chemistry/carbon-and-its-compounds/same_position_rule.svg" alt="Compound containing two substituents both will get either 3,4 or 4,3 positions respectively." caption="Any kind of numbering will produce 3,4 as positions only." >}}

Here, both the the `Left -> Right` and `Left <- Right` **numbering yields the
same positions**, namely 3,4.

The only difference is the substituents, they can be either :

- 3-Ethyl and 4-Methyl

or they can be:

- 3-Methyl and 4-Ethyl

The rule says the **correct answer** to that question is the first pair, i.e,
**3-Ethyl and 4-Methyl because alphabetically Ethyl comes first.** And thus, the
name of this compound will be :

- Position of substituents : 3,4
- Name of substituents: Ethyl, Methyl
- Wordroot: Hex (6 Carbons)
- Suffix : Ane (because only single bonds are present)

**3-Ethyl-4-Methylhexane**

### Functional Groups : Ketone vs. Aldehyde

Since we put the suffixes of functional groups at the end of the compound name,
for example, '-ol', it's important to know which functional groups has what
suffix.

Generally, at this level you only get a few functional groups, like alcohol,
aldehyde etc. One pair of functional groups, namely ketone and aldehyde are kind
of confusing since they have very similar structures. They **both contain a
carbonyl functional group**, i.e., C=O.

However, it's very easy to distinguish them:

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/ketone-and-aldehyde.svg" caption="Diagram representing the general formula for ketones and aldehydes" >}}

#### Aldehyde

If **at least one of the atoms bonded to the carbon atom** in carbonyl
functional group (C=O), **is a hydrogen** atom **then it's an aldehyde.**
Aldehydes have the **suffix _'-al'_**.

For example:

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/aldehyde.svg"  caption="Molecular structure of methanal" >}}

As can be seen from the structure, an aldehyde group has to come at the end of a
carbon chain.

#### Ketone

If **_both_ of the atoms of bonded** to the carbon in the C=O group **are not
hydrogen, then it's a ketone.** Ketones have the **suffix _'-one'_**

Unlike aldehydes, **ketones can come anywhere in the chain**, as shown here:

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/ketone.svg" caption="Molecular structure of acetone" >}}

## Combustion

The process of burning of a carbon compound in air to give carbon dioxide heat
and light is known as combustion. Simply it's just burning.

Alkanes produce a lot of heat, thus they are excellent fuels.

- Methane is a natural gas and it produces a lot of heat, therefore its used as
  fuel in homes, transport and industry.

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/methane_combustion_formula.svg" alt="Formula of methane reacting with oxygen" caption="" >}}

- The **LPG** cylinder contains **butane.**

- **Carbon compounds** are used as **fuel** because they **release a lot of
  heat.**

### Combustion in Saturated & Unsaturated Compounds

- **Saturated hydrocarbons burned completely giving a blue flame, given they
  have enough oxygen**, but if they **don't get sufficient oxygen they have
  incomplete combustion and give yellow, sooty, flame,**

- **Unsaturated hydrocarbons**, on the other hand, **give yellow, sooty flame
  irrespective of the amount of oxygen.**

### Flames

**Flames** on things like burning wood **are produced because volatile
substances** on it **vaporize** giving it a flame. It's is **only produced when
gaseous substances burn.**

**That's why things like charcoal don't produce a flame** because they don't
have any volatile or gaseous substance.

## Oxidization

Addition of {{< rawhtml >}}O<sub>2</sub>{{< /rawhtml >}} or removal of hydrogen
is called oxidization.

- **Oxidizing Agent:** Compound capable of adding oxygen to others.

Example: Alkaline **Potassium Permanganate**
({{< rawhtml >}}KMnO<sub>4</sub>{{< /rawhtml >}}) or acidified **Potassium
dichromate**
({{< rawhtml >}}K<sub>2</sub>Cr<sub>2</sub>O<sub>7</sub>{{< /rawhtml >}}).

### Oxidization in Alcohols

On oxidization, **alcohols turn into their corresponding acid.**

{{< figure align=center src="/images/chemistry/carbon-and-its-compounds/alcohol_oxidization.svg" alt="Diagram showing ethanol turning into ethanoic acid" caption="Ethanol turns into ethanoic acid on oxidization" >}}

As seen in the diagram above, **the carbon with the functional group OH, loses
its hydrogen** and that causes it to **forms a double bond with an oxygen
atom**, resulting in the corresponding alcohol.

## Addition Reaction

In addition reactions **unsaturated compounds react with a molecule** such as
{{< rawhtml >}}H<sub>2</sub>{{< /rawhtml >}},
{{< rawhtml >}}Cl<sub>2</sub>{{< /rawhtml >}},{{< rawhtml >}}Br<sub>2</sub>{{< /rawhtml >}}
etc. **to give saturated hydrocarbon compounds** as products.

- **Addition reactions are a characteristic property of unsaturated
  hydrocarbons**, i.e., of alkenes and alkynes.

Specifically, the **type of addition reaction** we are intrested in **is
hydrogenation.**

### Hydrogenation

**Catalytic hydrogenation is a form of addition reaction** in which **hydrogen
is added** **in the presence of a catalyst** such as nickel.

{{< figure src="/images/chemistry/carbon-and-its-compounds/addition_reaction.svg" alt="Ethene reaction with hydorgen to give Ethane in the presence of nickel at 573K." >}}

### Hydrogenation of Oils

The process of **hydrogenation reaction is commonly used to produce "vegetable
ghee."** Which is just hydrogenated vegetable oil.

{{< figure src="/images/chemistry/carbon-and-its-compounds/dalda.webp" alt="Picture of 'Dalda', a famous Indian brand of hydrogenated vegetable oil." caption="[Dalda](https://commons.wikimedia.org/wiki/File:Dalda_Vegetable_Ghee.jpg), the famous Indian brand of hydrogenated vegetable oil." >}}

The reaction used for this purpose typically goes like this:

{{< figure src="/images/chemistry/carbon-and-its-compounds/hydrogenation_of_oil_1.svg" alt="" caption="" >}}

Vegetable oils containing **_unsaturated_ fatty acids** are **good** for health.
Hydrogenation produces **saturated** trans **fats** in these vegetable oils
which have been linked to multiple heart diseases, causes obesity and is overall
**bad** for health.

### The Bromine Water Test

**A solution of bromine in water is called bromine water**. It is **red-brown in
color** because of the presence of bromine. **If an organic compound** manages
to **de-colorize bromine water** then **it is unsaturated**.

Saturated compounds don't de-colorize bromine water. This happens because, when
bromine water is added to an unsaturated organic compound, the `Br` element in
the solution reacts with the unsaturated organic compound and leaves the
solvent, resulting the the solution losing its color. Since bromine can't
perform the same reaction with saturated organic compounds, they don't
de-colorize the solution.

## Substitution Reaction

Substitution reactions are sort of the addition reactions of alkanes, in the
sense of being _their_ characteristic property. Since **saturated hydrocarbons**
have all their carbon atoms engaging in some kind of bond (that's why they are
called _saturated_ ) they are generally **pretty unreactive and stable.** But
they do take part in one specific type of reaction.

The reaction in which **one or more hydrogen atoms of a saturated hydrocarbon is
replaced by some other atoms.**

- **Substitution reactions are a characteristic property of saturated
  hydrocarbons**, i.e., of alkanes.

- **Substitution reaction involving chlorine is called chlorination.** This
  happens in the presence of sunlight. A substitution reaction with methane
  makes **chloromethane.**

## Important Compounds

### Ethanol

Ethanol, chemically
**{{< rawhtml >}}C<sub>2</sub>H<sub>5</sub>OH{{< /rawhtml >}}**, is a widely
used substance in organic chemistry.

- **Physical properties.**

    - It's a colorless liquid.
    - It has a pleasant smell.
    - The boiling point is 351K (78°C).
    - It's miscible with water.
    - It has no ions, therefore it's a non-conductor of electricity.

- **Chemical properties**

    - Oxidization by oxidant agents. As displayed
      [here](#oxidization-in-alcohols)

    - Reaction with sodium: Ethanol **reacts with sodium leading to the
      evolution of hydrogen gas.**

    - **Reaction to give ethene :** Heating ethanol at **443K** with excess
      concentrated sulphuric acid results in the dehydration of ethanol to give
      ethene. The concentrated sulphuric acid can be regarded as a dehydrating
      agent as **it removes water from ethanol.**

{{< figure src="/images/chemistry/carbon-and-its-compounds/ethanol_1.svg" alt="Chemical formula representing ethnol heated with sulphuric acid to give ethene" caption="Ethanol gives Ethene" >}}

- **Uses of Ethanol**

    **1.** It's **used in the manufacture** of paints, medicines, dyes,
    perfumes, soap, and synthetic rubber.

    **2.** **Ethanol is the active ingredient of beverages** like wine, beer,
    whiskey and other liquors.

    **3.** It's used an **antiseptic to sterilize wounds and syringes.**

    **4.** It's used as **antifreeze** in the radiators of vehicles in cold
    countries.

    **5.** It's used in **medicines** such as **cough syrup** etc.

- **Terms Related to Ethanol**

    **1. Rectified Spirit**: Ethanol containing **5% water.**

    **2. Absolute Alcohol**: 100% **pure ethanol.**

    **3. Denatured Alcohol**: Ethanol with poisonous substances, such as
    Methanol, to prevent drinking.

- **Effects of Ethanol on The Human Body**

The consumption of large quantities of ethanol results in:

- The slowing down of metabolic processes,
- Suppression of the nervous system.
- Lack of coordination
- Mental confusion, drowsiness.
- Lowering of consciousness and eventually being completely unconscious.

**It gives a sense of relaxation**, but it seriously impairs:

- Sense of judgment
- Sense of timing
- Muscular coordination

As mentioned previously, to stop the drinking of industrially produced ethanol,
various **poisonous substances** are mixed in with it, like **methanol** which
can quickly kill a human being. **This is called denatured alcohol.**

- It causes addiction
- It damages liver if taken regularly in large amounts.

### Ethanoic Acid

Commonly called **acetic acid**, ethanoic acid, chemically
**{{< rawhtml >}}CH<sub>3</sub>COOH{{< /rawhtml >}}**, is another important
substance borne out of organic chemistry.

- **Physical Properties**

    - **5%-8%** of solution **in water is vinegar.**

    - Used for **preserving food.**

    - **Boiling point** of pure ethanoic acid is **290K (17°C).** Therefore,
      it's also called **'glacial acetic acid'.**

- **Chemical Properties**

    - **Esterification Reaction:** Ethanoic acid reacts with ethanol in the
      presence of an acid catalyst to give an ester. This **process is called
      esterification.**

    - **Saponification Reaction:** Esters are further treated with **NaOH
      solution** get converted back to **alcohol and salt of carboxylic acid.**
      This is also **used for making soaps.**

    - Acid base reaction is just as usual. It gives salt and water.

    - Just like other acids, **ethanoic acid's reaction with carbonates or
      hydrogencarbonates gives a salt, carbon dioxide and water.**

## Soaps

A soap is a **sodium or potassium salt of long chain carboxylic acid**, also
known as fatty acids, having cleansing properties in water.

**Example:** Sodium stearate
**{{< rawhtml >}}(C<sub>17</sub>H<sub>35</sub>COONa){{< /rawhtml >}}**, Sodium
palmitate **{{< rawhtml >}}(C<sub>15</sub>H<sub>31</sub>COONa){{< /rawhtml >}}**

### Making Soap

Soap is **prepared by heating animal fat or vegetable oils with NaOH solution.**
They react with NaOH to produce soap and glycerol which is a sweetener.

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/soap_reaction.svg" alt="Fat or Oil heated with NaOH gives Soap + Glycerol" caption="Process of Soap-making" >}}

### Structure of Soap

Soaps are made of a hydrophilic head, i.e., a head that likes water and a
hydrophobic tail, i.e., a head that dislikes water. In the formula RCOONa, the R
is the long tail and the Na is the head.

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/structure_of_soap.svg" alt="Structure of a soap molecule" caption="Structure of a soap molecule" >}}

### How Do Soaps Clean?

If you look closely, the structure of one molecule of soap looks like a sperm!
That's because this is a common structure used on the molecular level for
movement.

The **tail dislikes water** and therefore **tries to avoid it.** But it's also
oil soluble, so it's attracted to oil. The **head likes water** and it **tries
to interact with water** as much as possible.

When a cloth is washed, the fats and oils in it are surrounded by soap molecules
in a specific structure called **'micelle'**, due the behaviour of the
molecule's head and tail.

{{< figure align=center  src="/images/chemistry/carbon-and-its-compounds/micelle.svg" alt="Micelle structure around a droplet of oil" caption="Micelle structure" >}}

As can be seen, the **tail arrange itself towards the oil molecule** and the
**head positions itself towards the water**, thus they sort of **trap oil**,
dirt etc. When the cloth is scrubbed in the soap solution, the oil and other
contaminants entrapped by the micelle get dispersed, **leaving the cloth clean**
and the solution dirty.

### Soaps in Hard Water

**Hard water contains calcium and magnesium salts**, that's what gives them the
name. When soap is mixed with hard
water,**{{< rawhtml >}}Ca<sup>2+<sup>{{< /rawhtml >}}** and
**{{< rawhtml >}}Mg<sup>2+<sup>{{< /rawhtml >}}** in the hard water **react with
soap to form insoluble calcium and magnesium salts of fatty acids.**

Basically,

**{{< rawhtml >}}C<sub>17</sub>H<sub>35</sub>COONa{{< /rawhtml >}}** +
**{{< rawhtml >}}MgCl<sub>2</sub>{{< /rawhtml >}}** ⟶
**{{< rawhtml >}}(C<sub>17</sub>H<sub>35</sub>COO)<sub>2</sub>Mg{{< /rawhtml >}}**

It can obviously also happen with calcium,

**{{< rawhtml >}}C<sub>17</sub>H<sub>35</sub>COONa{{< /rawhtml >}}** +
**{{< rawhtml >}}CaCl<sub>2</sub>{{< /rawhtml >}}** ⟶
**{{< rawhtml >}}(C<sub>17</sub>H<sub>35</sub>COO)<sub>2</sub>Ca{{< /rawhtml >}}**

This product at the end is formed as a **white precipitate** called **scum.**
Thus, **soap is ineffective in hard water.**

## Detergents

Detergents are **known as 'soapless soaps.'**

These are sodium salts of long chain benzene sulphonic acid or sodium salt of a
long chain alkyl hydrogen sulphate.

Example:
**{{< rawhtml >}}C<sub>17</sub>H<sub>35</sub>SO<sub>3</sub>Na{{< /rawhtml >}}**

### Detergents in Hard Water

**Detergents simply _do not react_ with calcium and magnesium salts present in
hard water to form insoluble precipitate** in the form of scum. Thus,
**detergents remain effective in hard water.**
