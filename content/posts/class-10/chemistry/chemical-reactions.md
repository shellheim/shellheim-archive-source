---
title: 'Chemical Reactions'
date: 2025-01-02T11:24:57+05:30
description: 'Learn about Chemical Reactions'
tags: ['Chemistry', 'Reaction', 'Chemicals', 'Notes']
draft: false
---

**The transformation of chemical substance into another chemical substance is
known as chemical reaction.**

For example: Rusting of iron, the setting of milk into curd, digestion of food,
respiration, etc.

## Parts of A Reaction

In a chemical reaction,the **new substances** formed **are completely different
in terms of chemical properties** from the original substance, so in a chemical
reaction.

**Therefore, chemical change takes place.** This is due to rearrangement of
atoms in a chemical reaction.

- The substances which take part in a chemical reaction are called reactants.
- The new substances produced as a result of a chemical reaction are called
  products.

## Burning of Magnesium

The burning of magnesium in the air to form magnesium oxide is an example of a
chemical reaction.

{{< figure src="/images/chemistry/chemical-reactions/magnesium_reaction.svg" caption="Magnesium Oxide formation" >}}

Before burning, the magnesium ribbon is **cleaned by rubbing with sandpaper.**
This is done **to remove** the protective layer of basic **magnesium carbonate
from the surface of the magnesium ribbon.**

{{< figure src="/images/chemistry/chemical-reactions/magnesium_burning.webp" caption="Magnesium giving off a sparkly light." >}}

Here, **the reactants are Magnesium and Oxygen** while the **product is
Magnesium Oxide.**

## Characteristics of Chemical Reactions

### Evolution of gas

The chemical reaction between zinc and dilute sulphuric acid is characterised by
the evolution of hydrogen gas.

{{< figure src="/images/chemistry/chemical-reactions/evolution_of_gas.svg" caption="Zinc and dilute sulphuric acid reacting, liberating hydrogen gas" >}}

### Change in Colour

Coloured potassium permanganate solution is characterised by a change in colour
from purple to colourless. The chemical reaction between sulphur dioxide gas and
acidified potassium dichromate solution is characterized by a change in colour
from orange to green.

### Change in state of substance

Characterised by a change in state from solid to liquid and gas (because the wax
is a solid, water formed by the combustion of wax is a liquid at room
temperature whereas, carbon dioxide produced by the combustion of wax is a gas).
There are some chemical reactions which can show more than one characteristics.

### Change in temperature

The chemical reaction between quick lime and water to form slaked lime is
characterized by a change in temperature (which is a rise in temperature). The
chemical reaction between zinc granules and dilute sulphuric acid is also
characterised by a change in temperature (which is a rise in temperature).

{{< figure src="/images/chemistry/chemical-reactions/heat_reaction.svg" caption="Quick lime reacting with water to form slaked lime" >}}

### Formation of Precipitate

The chemical reaction between sulphuric acid and barium chloride solution
results in the formation of a white precipitate of barium sulphate. This is a
type of **double displacement reaction**.

**Example Reaction:**

{{< figure src="/images/chemistry/chemical-reactions/precipitate.svg" caption="Barium Chloride and sulphuric Acid result in a precipitate Barium Sulphate" >}}

- **Key Takeaway**: Precipitation reactions occur when two aqueous solutions
  combine to form an insoluble solid (precipitate).

## Understanding Chemical Equations

A **chemical equation** represents the reactants and products in a chemical
reaction using symbols and formulas. This provides a concise and informative way
of expressing the reaction.

### General Format of a Chemical Equation

{{< figure src="/images/chemistry/chemical-reactions/general_formula.svg" alt="General formula of chemical reactions: A + B -> C + D" caption="General formula of chemical reactions" >}}

- **Reactants**: A and B
- **Products**: C and D
- **Arrow**: Indicates the direction of the reaction.

## Types of Chemical Equations

Chemical equations can be categorized into **balanced** and **unbalanced**:

### Balanced Chemical Equation

- **Definition**: The number of atoms of each element is equal on both sides of
  the equation.
- **Example**:

{{< figure src="/images/chemistry/chemical-reactions/evolution_of_gas.svg" caption="An example of a balanced chemical equation." >}}

- According to the **Law of Conservation of Mass**, mass is neither created nor
  destroyed in a chemical reaction, so the number of atoms should balance.

### Unbalanced Chemical Equation

- **Definition**: The number of atoms of each element differs on both sides.
- **Example**:

{{< figure src="/images/chemistry/chemical-reactions/unbalanced_equation.svg" alt="Reaction of Iron and Oxygen to produce Iron(II, II) Oxide." caption="An example of an unbalanced chemical equation." >}}

- To balance this, you must adjust the coefficients of each substance.

## Balancing a Chemical Equation

### Steps to Balance:

1. Write the equation:

{{< figure src="/images/chemistry/chemical-reactions/unbalanced_equation.svg" alt="Reaction of Iron and Oxygen to produce Iron(II, II) Oxide." caption="An example of an unbalanced chemical equation." >}}

2. Create a table to track the number of atoms on both sides.

| Name of Atom | No. of Atoms in Reactant | No. of Atoms in Product |
| ------------ | ------------------------ | ----------------------- |
| Iron         | 1                        | 3                       |
| Hydrogen     | 2                        | 2                       |
| Oxygen       | 1                        | 4                       |

3. Now, remember, **only coefficients can be changed** and **_not_** the
   subscripts.

4. Multiply with coefficients until both sides have the **same number of the
   same kind of atoms.**

| Name of Atom | No. of Atoms in Reactant | No. of Atoms in Product |
| ------------ | ------------------------ | ----------------------- |
| Iron         | 1 \* 3 = 3               | 3                       |
| Hydrogen     | 2 \* 4 = 8               | 2 \* 4 = 8              |
| Oxygen       | 1 \* 4 = 4               | 4                       |

In balancing oxygen, we also increased the number of hydrogen on the LHS from 2
to 8, so we have to account for that on the RHS and add a 4 there as well.

**Final balanced equation:**

{{< figure src="/images/chemistry/chemical-reactions/balanced_equation.svg" alt="Reaction " caption="An example of an unbalanced chemical equation." >}}

## Making Chemical Equations More Informative

You can make chemical equations more informative by including the **physical
states** of the substances and any **conditions** under which the reaction
occurs.

- **States**:

    - Solid = (s)
    - Liquid = (l)
    - Gas = (g)
    - Aqueous solution = (aq)

- **Conditions**: Write them above or below the arrow.

## Types of Chemical Reactions

Chemical reactions are classified into various types based on how the reactants
and products interact:

### Combination Reaction

- **Definition**: Two or more reactants combine to form a single product.
- **Example**:

{{< figure src="/images/chemistry/chemical-reactions/magnesium_reaction.svg" caption="Magnesium Oxide formation" >}}

### Decomposition Reaction

- **Definition**: A single compound breaks down into two or more simpler
  substances.
- **Example**:

**1. Calcium Carbonate breaking down into Calcium Oxide.**

{{< figure src="/images/chemistry/chemical-reactions/composition_reaction_1.svg" caption="Calcium Carbonate breaking down into Calcium Oxide and Carbon dioxide" >}}

**2. When Ferric Gydroxide is heated, it decomposes into Ferric Oxide and
water.**

{{< figure src="/images/chemistry/chemical-reactions/composition_reaction_2.svg" caption="Ferric Oxide is heated, it decomposes into Ferric Oxide and water." >}}

- **Types of Decomposition**

**1. Thermal**: Heat causes decomposition.

{{< figure src="/images/chemistry/chemical-reactions/thermal_reaction.svg" caption="Lead Nitrate breaking down into Lead Oxide, Nitrogen dioxide and Oxygen." >}}

**2. Electrolytic**: Electricity causes decomposition.

{{< figure src="/images/chemistry/chemical-reactions/electrolytic_reaction.svg" caption="Water breaking down down into Hydrogen and Oxygen after electrolysis." >}}

**3. Photolysis**: Sunlight causes decomposition.

{{< figure src="/images/chemistry/chemical-reactions/photolysis_reaction.svg" caption="Water breaking down down into Hydrogen and Oxygen after electrolysis." >}}

### Displacement Reaction

- **Definition**: One element replaces another in a compound.
- **Example**:

Zinc + Copper Sulfate → Zinc Sulfate + Copper

### Double Displacement Reaction

- **Definition**: Ions of two compounds exchange places to form new compounds.
- **Example**:

{{< figure src="/images/chemistry/chemical-reactions/precipitate.svg" caption="Barium Chloride and sulphuric Acid result in a precipitate Barium Sulphate" >}}

- **Types:**

**1. Precipitation Reaction**: A type of double displacement reaction where a
precipitate forms. Like the example of Barium Chloride and Sulphuric Acid
[above](#double-displacement-reaction).

**2. Neutralization Reaction:** The reaction in which an acid reacts with a base
to form salt and water by an exchange of ions is called neutralization Reaction.

{{< figure src="/images/chemistry/chemical-reactions/neutralization_reaction.svg" caption="Sodium Hydroxide and Hydrochloric Acid reacting to form common salt and water." >}}

### Oxidation and Reduction Reactions

- **Oxidation**: The addition of oxygen or the loss of hydrogen.
- **Reduction**: The loss of oxygen or the gain of hydrogen.

The **reaction in which oxidation and reduction** both **happen simultaneously**
is called **Redox reaction.**

1. **Agents**

    - The substance **which gives oxygen** for oxidation is called an
      **oxidizing agent.**
    - The substance **which gives hydrogen** for reduction is called a
      **reducing agent.**

{{< figure src="/images/chemistry/chemical-reactions/redox.svg" alt="Copper Oxide is heated with hydrogen to form Copper metal and water." caption="Copper gets reduced while Hydrogen gets oxidized." >}}

## Exothermic vs. Endothermic Reactions

### Exothermic Reactions:

- **Definition**: Reactions that release energy, typically in the form of heat.
- **Example**:

{{< figure src="/images/chemistry/chemical-reactions/exothermic_reaction.svg" caption="Carbon and Oxygen react to form Carbon dioxide and release heat, making this reaction exothermic." >}}

### Endothermic Reactions

- **Definition**: Reactions that absorb energy, typically in the form of heat.
- **Example**:

{{< figure src="/images/chemistry/chemical-reactions/endothermic_reaction.svg" caption="Zinc Carbonate heated to breakdown into Zinc Oxide and Carbon dioxide" >}}

## Oxidation Reactions in Daily Life

Oxidation is either the _gain_ of oxygen or the _loss_ of hydrogen.

### Corrosion

- **Definition**: The slow conversion of metals into undesirable compounds due
  to their reaction with air, moisture, or acids.
- **Example**: Rusting of iron (iron reacts with oxygen and moisture to form
  iron oxide).

**Methods to Prevent Corrosion**

**1.** Painting

**2.** Greasing and oiling

**3.** Galvanization

### Rancidity

- **Definition**: The undesirable change in taste and odor of food containing
  fats and oils due to oxidation.

**Methods to Prevent Rancidity**

**1.** Adding antioxidants

**2.** Vacuum packing

**3.** Nitrogen packing

**4.** Refrigeration
