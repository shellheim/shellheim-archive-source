---
title: 'Volumne and Surface Areas'
date: 2024-11-20T18:17:25+05:30
draft: false
---

{{< rawhtml >}}

<style>
img {
    border:none;
    max-width:60%;
}
mfrac {
    font-size:2rem;
}
</style>

{{< /rawhtml >}}

This is a just reference table for memorization. The Prism _might_ be
unnecessary.

| Shape      | Figure                            | Surface Area Formula | Lateral Surface Area Formula | Volume Formula                                                                                                                  |
| ---------- | --------------------------------- | -------------------- | ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| Cube       | ![](/images/maths/cube.svg)       | 6a²                  | 4a²                          | a³                                                                                                                              |
| Cuboid     | ![](/images/maths/cuboid.svg)     | 2(lb + lh + bh)      | 2h(l + b)                    | l × b × h                                                                                                                       |
| Sphere     | ![](/images/maths/sphere.svg)     | 4πr²                 | Not Applicable               | {{< rawhtml>}} <math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow>4</mrow><mrow>3</mrow></mfrac>{{< /rawhtml >}}πr³  |
| Hemisphere | ![](/images/maths/hemisphere.svg) | 3πr²                 | 2πr²                         | {{< rawhtml>}} <math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow>2</mrow><mrow>3</mrow></mfrac>{{< /rawhtml >}}πr³  |
| Cylinder   | ![](/images/maths/cylinder.svg)   | 2πr(h + r)           | 2πrh                         | πr²h                                                                                                                            |
| Cone       | ![](/images/maths/cone.svg)       | πr(r + l)            | πrl                          | {{< rawhtml>}} <math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mrow>1</mrow><mrow>3</mrow></mfrac>{{< /rawhtml >}}πr²h |
