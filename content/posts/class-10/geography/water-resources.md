---
title: 'Water Resources'
date: 2024-09-01T12:29:53+05:30
tags: ['Geography', 'SST', 'Resource', 'Water', 'Harvesting']

draft: false
---

## The Water Problem

In India we either experience water scarcity ourselves or hear about it in the
news every summer. This has been a perennial problem since colonial times.
What's different is how we imagine "scarcity". That conjures up an image of sand
dunes and Rajasthani women carrying many _matkas_ on their heads.

{{< figure align=center  src="/images/geography/water-resources/dal_lake.webp" alt="Traditional Wooden Houseboats Boats on Dal Lake between Mountains in Kashmir" caption="[Dal Lake, Jammu & Kashmir](https://www.pexels.com/photo/traditional-wooden-houseboats-boats-on-dal-lake-between-mountains-in-kashmir-26752641/)" >}}

But the water problem isn't only centered on geographical problems such as low
precipitation. It's penetrated the urban centers and rural areas which in the
past had plenty of water.

This prompted the government to come with solutions that seemed fit to it. The
chapter basically outlines the genesis of **Multi-purpose projects** aka Dams
and the problems associated with them. Then it goes on to talk about water
harvesting and its various forms in India. Frankly, the solutions feel outdated
in the chapter. The investment in our water infra is abysmal for mid to small
cities and non-existent for towns. That's the biggest issue rather than dams or
water harvesting.

### Circumstances of Scarcity

You have two situations of scarcity :

**1. Low quantity of Water**

India has both problems in different regions. The increasing urban concentration
has aggravated the problems of water access due to availability issues. The
population needs more water for not only drinking but other jobs such as
cleaning and irrigation which is the largest consumer of water. This leads to
installment of personal water pumps for houses and tube-wells for farms in the
absence of a public water distribution system.

This leads to over-exploitation of an already mismanaged, precious resource.
After Independence, India's water needs increased significantly due to :

-   Concentration of Population in Cities
-   Industrialization
-   Irrigation
-   Hydroelectric Plants

**2. High quantity of Water but Bad Management**

In the absence of a good water distribution system, the people are also
vulnerable to various water borne diseases. To tackle this issue in rural areas,
the **government** started the **scheme** called **Jal Jeevan Mission** (JJM) to
ensure that every rural household gets **55 liters of water** per capita per day
on a long-term basis.

If the scheme doesn't go as planned and the exploitation continues then this
will result in ecological disaster.

## Multi-Purpose River Projects

> A dam is a barrier across flowing water that obstructs, directs or retards the
> flow, often creating a reservoir, lake or impoundment. “Dam” refers to the
> reservoir rather than the structure. Most dams have a section called a
> spillway or weir over which or through which it is intended that water will
> flow either intermittently or continuously. Dams are classified according to
> structure, intended purpose or height. Based on structure and the materials
> used, dams are classified as timber dams, embankment dams or masonry dams,
> with several subtypes. According to the height, dams can be categorised as
> large dams and major dams or alternatively as low dams, medium height dams and
> high dams.

Multi-purpose projects or Multi-purpose _river_ projects generally refer to
dams. They're called 'multi-purpose' due to their use in :

-   irrigation
-   power generation
-   industrial water supply
-   recreational activities
-   inland navigation
-   [fish breeding](https://paste.rs/20VCV.txt)

For example:

**Sutluj-Beas river basin's** Bhakra-Nangal project uses its water for
hydel-power generation and irrigation.

Similarly, the **Hirakund Dam** helps with conservation of water and flood
control.

Initially the dams were seen as the conerstone on which the progress of India
was to be laid. But eventually, the problems around started coming to light.

> Dams are the temples of modern India. — Jawaharlal Nehru

In recent years they've come under scrutiny due to :

-   Displacement of locals
-   Causing ecological damage
-   Poor sedimentation flow causing land fertility issues
-   Rockier sea beds due to excessive sedimentation at the bottom of the
    reservoir
-   Submerging existing flora and soil leading to their decomposition
-   Inducing earthquakes, causing pollution, pests and water borne diseases

Due to high availability of water in some states, farmers have changed cropping
patterns to prefer water intensive crops which causes further overuse.

## Rainwater Harvesting

The disadvantages of multi-purpose projects points to another probable solution,
that is rain water harvesting. This practice has been followed for centuries in
India. Like in :

-   The **'Guls' or 'Kuls'** of the **Western Himalayas** for **agriculture.**
-   **Rooftop rainwater harvesting** to store drinking water **in Rajasthan**.
-   In the flood plains of **Bengal**, people built **flood channels to
    irrigate** their **fields.**
-   **‘Khadins’** in **Jaisalmer** and **‘Johads’** in other parts of
    **Rajasthan.**

### _Tankas_

In the semi-arid areas of Rajasthan like Bikaner, Barmer etc. traditionally all
houses had tanks for water storage, called _tankas_. These tankas could be as
big as a room. The largest found was 6.1m × 4.27m × 2.44m.

These _tankas_ were connected with an rainwater harvesting system, generally
built inside the house. The rainwater on the flat roofs was supposed to trickle
down into these _tankas_ through pipes.

The first rain isn't collected as this would clean the roofs and the pipes.
Subsequent showers are then collected.

This rainwater, also called **_palar pani_**, is considered the purest form of
water. These reservoirs would also keep their adjoining rooms cool. Though this
practice is fast disappearing in Rajasthan due the Indira Gandhi Canal. However,
it's being adopted in many other rural and urban parts of India.

> NOTE : Tamil Nadu is the first state in India which has made rooftop rainwater
> harvesting structure compulsory to all the houses across the state. There are
> legal provisions to punish the defaulters.

In **Gendathur**, a remote **backward village in Mysuru, Karnataka**, villagers
have rainwater harvesting systems on their roofs.

Nearly **200 households have installed this system** and the village has earned
the rare distinction of being rich in rainwater.

Gendathur **receives an annual precipitation of 1,000 mm**, and with **80 per
cent collection efficiency** and **of** about **10 fillings**, every house can
**collect** and use about **50,000 litres** of water **annually.**

From the **200 houses**, the net **amount of rainwater** harvested **annually**
amounts to **1 lakh litres.**

{{< figure align=center  src="/images/geography/water-resources/water_harvesting.webp" alt="Image featuring the bamboo irrigation system of Meghalaya" caption="Bamboo Drip Irrigation System" >}}
