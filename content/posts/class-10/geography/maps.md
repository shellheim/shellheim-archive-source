---
title: 'Maps'
date: 2025-02-23T11:55:27+05:30
draft: false
---

Thank the lord for the existence of [excalidraw.](https://excalidraw.com)

## History

### Gujrat

![Map](/images/geography/maps/gujrat.webp)

- Dandi is where **Gandhi Ji broke the salt law.**
- In Ahemdabad, there was a cotton mill worker strike, organized by Gandhi Ji.

### Bihar

![Map](/images/geography/maps/champaran.webp)

- Champaran is where **Gandhi Ji organized a struggle against the oppressive
  plantation system for indigo.** Like the 3-kathia act.

### Congress Sessions and Jallianwala

![Map](/images/geography/maps/national.webp)

## Geograhy

### Soil

Some **major producers of crops**, largest producer written first.

- **Rice:** West Bengal, Uttar Pradesh
- **Wheat:** Uttar Pradesh ,Punjab, Haryana
- **Sugarcane:** Uttar Pradesh
- **Tea:** Assam, West Bengal
- **Coffee:** Karnataka, Kerala
- **Cotton:** Gujarat, Maharashtra
- **Rubber:** Kerala
- **Jute:** West Bengal

![](/images/geography/maps/soil.webp)

### Rice Distribution

![](/images/geography/maps/rice.webp)

### Wheat Distribution

![](/images/geography/maps/wheat.webp)

### Dams

![](/images/geography/maps/dam.webp)

### Sea Ports

![](/images/geography/maps/ports.webp)

### Internationl Airports

![](/images/geography/maps/airports.webp)

### Techonology Parks

![](/images/geography/maps/parks.webp)

### Thermal Power Plants

![](/images/geography/maps/thermal.webp)

### Nuclear Power Plants

![](/images/geography/maps/nuclear.webp)

### Oil Fields

![](/images/geography/maps/oil.webp)

### Iron and Steel Plants

![](/images/geography/maps/steel.webp)

### Iron Mines

![](/images/geography/maps/iron.webp)

### Coal Mines

![](/images/geography/maps/coal.webp)
