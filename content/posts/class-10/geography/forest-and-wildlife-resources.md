---
title: 'Forest and Wildlife Resources'
date: 2024-07-14T19:33:41+05:30
tags: ['Geography', 'SST', 'Forest', 'Soil', 'Land']
draft: false
---

This chapter deals with mainly :

1. Why and how of conservation
2. Government programmes about conservation
3. The local community's involvement in such efforts

## Biodiversity

Biodiversity, or biological diversity, is the variety of flora and fauna, in
their form and function, found on earth. They are quite different in their
looks, behaviour with other life, survival techniques etc. but they're are
interdependent on each other in a vast network of life.

It's important to protect biodiversity around us because it doesn't only serve
the aesthetics of an ecosystem but it's crucial for our survival as well. For
example different crops provide different regions in India with sufficient food
and sectors like fish trade relies on aquatic biodiversity. We also need
thriving diverse forests to counter the growing threat of the climate emergency.

{{< figure align=center  src="/images/geography/forest-and-wildlife-resources/fungi_of_saskatchewan.webp" alt="Image featuring the multiple types of fungi found in Saskatchewan, Canada." caption="[Fungi of Saskatchewan, Canada.](https://commons.wikimedia.org/wiki/File:Fungi_of_Saskatchewan.JPG)" >}}

## Conservation Efforts

Conservation is the study of the loss of biodiversity on Earth and of the
policies that can prevent this loss. Since the 60s and 70s Indian
conservationists demanded a national wildlife protection programme which
resulted in **The Indian Wildlife (Protection) Act, 1972.**

This act :

- made a list of protected species
- banned their hunting
- gave legal protection to their habitats
- restricted wildlife trade

The central government also took initiatives to protect specific wildlife, such
as:

- The Tiger
- The Kashmiri Hangul
- Freshwater & Saltwater crocodile and the _Gharial._
- The one-horned rhinoceros.
- The Asiatic Lion
- The Snow Leopard

Not only does the protective list feature animals in the conventional sense but
it also gained insects in 1980 and plants in 1991.

## Forests

Forests are classified into different categories to better manage them. These
categories are :

### Reserved Forests

These are considered as the most valuable forests from a conservation and
biodiversity perspective. So, these are the most heavily regulated forests. All
outside activities are banned here like hunting, grazing etc. These make up
**more than half** of total Indian forests.

### Protected Forests

These forests are one rank below reserved forests and have relatively less
strict rules governing them. This forest land is protected from further erosion
and depletion. These make up **almost one-third** of all Indian forests.

### Unclassed Forests

Forests belonging to none of the above two categories.

Reserved and Protected Forests are called **Permanent Forests.**

Madhya Pradesh has the highest percentage of permanent forests out of total
forest area at 75%!

## Communities and Forests

In popular Indian culture, our indigenous tribes are often marginalized. It's
important to realise that the only sustainable way to maintain our forest
ecosystems and wildlife is by including the communities who resided, in these
very ecosystems, for thousands of years conservation efforts!

There has been an endeavour close to this in the **JFM** (Joint Forest
Management) programme **launched in 1988.** First passed in Odisha, this
programme makes available the necessary systems of governance that include the
biggest stakeholders in forest decisions.

JFM makes village units for protection of forests. The villagers receive
benefits like non-timber forest produce and a share in the timber from
sustainable sources through successful production.

But we must do more to give these villages more power over decisions until
they're capable to make the ultimate decisions and have the autonomy to favour
only pro-forest and pro-people initiatives to take place.

Some successful examples of people conserving their lands are :

1. **Chipko Movement**

2. **Beej Bachao Andolan**

3. **Navdanya**
