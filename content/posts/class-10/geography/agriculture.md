---
title: 'Agriculture'
date: 2024-09-12T15:02:08+05:30
tags: ['Geography', 'SST', 'Agriculture', 'Soil']
draft: false
---

About 60% (two-thirds) of Indians depend either independently or directly on the
agricultural sector. We have large swaths of farmland and much of it has come up
after independence & specifically in the 60s, during the **green revolution.**

{{< figure align=center  src="/images/geography/agriculture/farmland.webp" alt="Green rice plant with full panicles" caption="[Green Field](https://www.pexels.com/photo/green-grass-field-946186/)" >}}

## Types of Agriculture

Many types of farming exist in the country, they differ on two axes,
technological and economical from subsistence—commercial to primitive—intensive.

### Primitive Subsistence Farming

Primitive subsistence farming is the farming done by primitive tools to sustain
life.

#### Location

On small patches of land, done in some parts of rural India for subsistence.

#### Dependence

Dependent on natural factors like: soil fertility, rain patterns etc.

#### Method

This agriculture is also called 'slash & burn' agriculture due to the fact that
farmers cultivate a small patch of land for grains and them set fire to them
leave them fallow for a few years. This heals the land and replenishes its
nutrients while the farmers clear another patch of land for farming.

This has different names like :

##### In India:

**_Jhumming_** in : Assam, Meghalaya, Mizoram and Nagaland

**_Pamlou_** in Manipur

**_DipDipaa_** in Bastar, Chhattisgarh

**_Bewar_** or **_Dahiya_** in Madhya Pradesh

**_Podu_** or **_Penda_** in Andhra Pradesh

**_Pama Dabi_** or **_Koman_** **_Bringa_** in Odisha

**Kumari** in Western Ghats

**_Valre_** or **_Waltre_** in South-eastern Rajasthan

**_Khil_** in the Himalayan belt

**_Kuruwa_** in Jharkhand

##### Internationally :

**_Milpa_** in Mexico and Central America

**_Conuco_** in Venzuela

**_Roca_** in Brazil

**_Masole_** in Central Africa

**_Ladang_** in Indonesia

**_Ray_** in Vietnam

### Intensive Subsistence Farming

Intensive subsistence farming is farming done for subsistence with extra
bio-chemical inputs and intensive labour input.

Since the land holdings in India are very small and uneconomical on a personal
level the farmers keep trying to get the max output from the limited land. Thus,
there is enormous pressure on agricultural land.

### Commercial Farming

Commercial farming is farming done with the aim of selling and making a profit
on the produce. It's characterized by the use of high yielding variety (HYV)
seeds, chemical fertilisers, insecticides and pesticides for higher
productivity.

Some crops are subsistence crops in one part of the country (like rice in
Odisha) and commercial crops in another part of the country (like Haryana and
Punjab).

#### Plantations

A plantation is a mix of industry and farming. It's capital and labour
intensive. Large swaths of lands are acquired and furnished with capital to grow
one kind of crop (like coffee, tea etc.). They are tendered to by migrant
labour. Plantations generally provide raw materials for other industries.

Important plantation crops in India include : tea, coffee, rubber, sugarcane,
banana.

Places they grow at : Tea in Assam and North Bengal. Coffee in Karnataka.

## Cropping Pattern

India's diversity is also showcased in its cropping pattern.

Indian has **three** principal **cropping seasons.** They are :

**1. Rabi**

Rabi crops are **sown in winter** from October to December and **harvested in
summer** from April to June.

Some **important rabi crops** are : wheat, barley, peas, gram and mustard.

States where **they are mostly grown** : Punjab, Haryana, Himachal Pradesh,
Jammu and Kashmir, Uttarakhand and Uttar Pradesh.

Western temperate cyclones help with precipation in winter months. Rabi crops
were also helped in Punjab and Haryana by the green revolution.

**2. Kharif**

Kharif crops are **sown at the onset of monsoon** in different parts of the
country and these are **harvested in winter** during September-October.

Some **important kharif crops** are : paddy, maize, jowar, bajra, tur (arhar),
moong, urad, cotton, jute, groundnut and soyabean.

States **where it's grown at** are : Assam, West Bengal, coastal regions of
Odisha, Andhra Pradesh, Telangana, Tamil Nadu, Kerala and Maharashtra,
particularly the (Konkan coast) along with Uttar Pradesh and Bihar.

It has become important in Punjab and Haryana as commercial crops too.

Assam, West Bengal and Odisha grow three crops of paddy, called **Aus, Aman and
Boro.**

**3. Zaid**

Zaid is a cropping season between kharif and rabi. Mostly from March to June.

Most crops in zaid are related to horticulture, like : water melon, muskmelon,
cucumber, vegetables and fodder crops. Sugarcane are also grown, though they
take upto a year.

## Major Grain Crops

Major crops grown in India include **both food and non-food** crops like wheat,
paddy, cotton, jute etc. Major crops include : rice, wheat, millets, pulses,
tea, coffee, sugarcane, oil seeds, cotton and jute, etc.

### Rice

-   Staple food crop of majority of Indias. **India** is its **second largest
    producer** after China.

-   It's a **kharif crop.**

-   **Requires** :

    1. High temperature, (above 25°C)
    2. High humidity
    3. Annual rainfall above 100 cm.

-   **Grown in** :

    1. Plains of north and north-eastern India.

    2. Coastal areas and the deltaic regions.

    3. Punjab, Haryana and western Uttar Pradesh and parts of Rajasthan grow it
       with the **help of canal irrigation and tubewells.**

### Wheat

-   The **second most important cereal** crop after rice.

-   It's a **rabi crop.**

-   It's the **main food crop, in north and north-western** part of the country.

-   **Requires :**

    1.  A cool growing season and a bright sunshine at the time of ripening.

    2.  75 cm of annual rainfall evenly-distributed over the growing season.

-   **Growing Zones**

    1. The **Ganga-Satluj plains** **in** the **north-west** and **black soil
       region of** the **Deccan.**

-   Major Producers : Punjab, Haryana, Uttar Pradesh, Madhya Pradesh, Bihar and
    Rajasthan.

### Millet

-   **Types**: Jowar, Bajra, Ragi
-   **Nutritional Value**: High in iron, calcium, micro nutrients, and roughage
-   **Jowar**:
    -   Third most important food crop
    -   Rain-fed, needs minimal irrigation
    -   Major states: Maharashtra, Karnataka, Andhra Pradesh, Madhya Pradesh
-   **Bajra**:
    -   Thrives on sandy and shallow black soils
    -   Major states: Rajasthan, Uttar Pradesh, Maharashtra, Gujarat, Haryana
-   **Ragi**:
    -   Grows in dry regions and various soil types
    -   Major states: Karnataka, Tamil Nadu, Himachal Pradesh, Uttarakhand,
        Sikkim, Jharkhand, Arunachal Pradesh

### Maize

-   **Uses**: Food and fodder
-   **Season**: Kharif crop, sometimes grown in rabi season in Bihar
-   **Growing Conditions**: Requires 21°C to 27°C, grows well in old alluvial
    soil
-   **Modern Inputs**: HYV seeds, fertilizers, irrigation
-   **Major states**: Karnataka, Madhya Pradesh, Uttar Pradesh, Bihar, Andhra
    Pradesh, Telangana

### Pulses

-   **Significance**: Largest producer and consumer in the world
-   **Nutritional Role**: Major protein source in vegetarian diets
-   **Types**: Tur (arhar), Urad, Moong, Masur, Peas, Gram
-   **Growing Conditions**: Low moisture needs, leguminous (except arhar, which
    doesn't fix nitrogen)
-   **Major states**: Madhya Pradesh, Rajasthan, Maharashtra, Uttar Pradesh,
    Karnataka

## Major Non-Grain Crops

### Sugarcane

-   **Climate**: Tropical and subtropical, thrives in hot and humid conditions
-   **Temperature**: 21°C to 27°C
-   **Rainfall**: 75cm to 100cm annually; requires irrigation in low rainfall
    areas
-   **Soil**: Grows well on various soils
-   **Labour**: Requires manual labour from sowing to harvesting
-   **Production**: India is the second largest producer after Brazil
-   **Products**: Sugar, gur (jaggery), khandsari, molasses
-   **Major states**: Uttar Pradesh, Maharashtra, Karnataka, Tamil Nadu, Andhra
    Pradesh, Telangana, Bihar, Punjab, Haryana

### Groundnut

-   **2018 Ranking**: Second largest producer after China
-   **Other Oilseeds**: Mustard, coconut, sesamum (til), soybean, castor seeds,
    cotton seeds, linseed, sunflower
-   **Uses**: Cooking oil, raw materials for soap, cosmetics, and ointments
-   **Crop Seasons**:
    -   **Kharif Crop**: Groundnut, sesamum (in north India)
    -   **Rabi Crop**: Linseed, mustard, sesamum (in south India)
    -   **Both**: Castor seed
-   **Major states (2019–20)**: Gujarat (largest), followed by Rajasthan and
    Tamil Nadu

### Tea

-   **Cultivation**: Plantation agriculture
-   **Climate**: Tropical and subtropical, deep, fertile, well-drained soil,
    warm and moist
-   **Labour**: Labour-intensive, requires abundant, cheap, skilled labour
-   **Processing**: Done within tea gardens to maintain freshness
-   **Major states**: Assam, Darjeeling and Jalpaiguri (West Bengal), Tamil
    Nadu, Kerala, Himachal Pradesh, Uttarakhand, Meghalaya, Andhra Pradesh,
    Tripura
-   **2018 Ranking**: Second largest producer after China

### Coffee

-   **Quality**: Known for high quality, especially Arabica variety
-   **Origin**: Brought from Yemen
-   **Cultivation Areas**: Baba Budan Hills, Nilgiri (Karnataka, Kerala, Tamil
    Nadu)

### Horticulture Crops

-   **2018 Ranking**: Second largest producer of fruits and vegetables after
    China
-   **Fruits**:
    -   **Mangoes**: Maharashtra, Andhra Pradesh, Telangana, Uttar Pradesh, West
        Bengal
    -   **Oranges**: Nagpur, Cherrapunjee (Meghalaya)
    -   **Bananas**: Kerala, Mizoram, Maharashtra, Tamil Nadu
    -   **Lychee and Guava**: Uttar Pradesh, Bihar
    -   **Pineapples**: Meghalaya
    -   **Grapes**: Andhra Pradesh, Telangana, Maharashtra
    -   **Apples, Pears, Apricots, Walnuts**: Jammu and Kashmir, Himachal
        Pradesh

## Non-Food Crops

### Fibre Crops

-   **Major Types**: Cotton, Jute, Hemp, Natural Silk
-   **Sources**:
    -   **Cotton, Jute, Hemp**: Derived from crops grown in soil
    -   **Silk**: Obtained from silkworm cocoons, which are fed on mulberry
        leaves
-   **Silk Production**: Known as sericulture

### Cotton

-   **Origin**: Believed to be native to India
-   **Uses**: Key raw material for the cotton textile industry
-   **2017 Ranking**: Second largest producer after China
-   **Growing Conditions**:
    -   **Soil**: Drier black cotton soil of the Deccan Plateau
    -   **Climate**: High temperature, light rainfall or irrigation, 210
        frost-free days, bright sunshine
    -   **Season**: Kharif crop, matures in 6 to 8 months
-   **Major States**: Maharashtra, Gujarat, Madhya Pradesh, Karnataka, Andhra
    Pradesh, Telangana, Tamil Nadu, Punjab, Haryana, Uttar Pradesh

### Rubber

-   **Growing Conditions**:
    -   **Climate**: Moist and humid, rainfall > 200 cm, temperature > 25°C
    -   **Regions**: Equatorial, tropical, and sub-tropical areas
-   **Major States**: Kerala, Tamil Nadu, Karnataka, Andaman and Nicobar
    Islands, Garo Hills (Meghalaya)

## Technological & Institutional Reforms

Sustained uses of land without compatible techno-institutional changes have
hindered the pace of agricultural development. For a growing population, this
poses a serious challenge. Agriculture which provides livelihood for more than
60 per cent of its population, needs some serious technical and institutional
reforms.

Some reforms include :

**1. Abolition of Zamindaari**

However the implementation of this act was pretty bad.

**2. Collectivisation & consolidation of land holdings.**

Some **government scheme**s like **Kissan Credit Card (KCC)**, **Personal
Accident Insurance Scheme (PAIS)** were introduced for the benefit of the
farmers.

## Bhoodan Movement: The Bloodless Revolution

**Mahatma Gandhi declared Vinoba Bhave as his spiritual heir** and a key
participant in Satyagraha. Bhave was a staunch supporter of Gandhi’s concept of
gram swarajya. After Gandhi’s death, Bhave embarked on a padyatra to spread
Gandhi's message across India.

**During a lecture in Pochampalli, Andhra Pradesh, landless villagers requested
land for their economic well-being.** Although Bhave couldn’t immediately
promise land, he assured them he would discuss it with the Government of India
if they agreed to cooperative farming.

**In response, Shri Ram Chandra Reddy offered 80 acres of land for distribution
among 80 landless villagers, an act known as ‘Bhoodan’.** Bhave continued to
spread his ideas throughout India. Some zamindars offered entire villages to the
landless, known as Gramdan. Many landowners, motivated by the fear of land
ceiling laws, chose to donate portions of their land.
