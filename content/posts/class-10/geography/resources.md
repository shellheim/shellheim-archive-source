---
title: 'Resources'
date: 2024-05-01T17:22:58+05:30
tags: ['Geography', 'SST', 'Resource', 'Soil', 'Minerals', 'Soil Erosion']
draft: false
---

**Everything available in the environment which can be used to satisfy human
needs, provided, it is technologically accessible, economically feasible and
culturally acceptable can be termed as "resource".**

## Classification of Resources

Resources come in many different shapes and sizes, from different types of lands
and used by different sections of industries in, different ways by multiple
cultures.

Therefore, there are many classifications of resources based on such things,
like :

1. On the basis of **origin** – biotic and abiotic

2. On the basis of **exhaustibility** – renewable and non-renewable

3. On the basis of **ownership** – individual, community, national and
   international

4. On the basis of **status of development** – potential, developed stock and
   reserves.

{{< figure align=center src="/images/geography/resources/types_of_resources.webp" alt="Diagram showcasing the types of resources" caption="Types of Resources" >}}

## Problems of Resource Development

When we refer to 'resource development', it actually means the exploitation of
resources. It refers to methods we use to extract, process and distribute the
resource. The traditional thinking, for a long time had been that resources were
unlimited and man could exploit them as much as he liked.

This began to completely change during the 1970s-1980s due to various factors
like increasing environmentalism, greater conservation efforts by governments
etc. This led to the, now prominent, theory of conservationism.

There are several **problems that stemmed from the indiscriminate use of natural
resources**, such as :

- Depletion of resources for corporate greed, benefiting a few.

- Unequal distribution of resources and increasing the gulf separating the rich
  and poor.

- Global ecological crises such as, climate change, ozone layer depletion,
  environmental pollution and land degradation.

## Resource Planning

Due to the increasing effects of the problems outlined above, there rose a need,
greater than ever before, of planning the development of resources in a
sustainable manner.

Resource planning is specially important in a country like India. Where there
are diverse cohorts of different, rich resources across a vast exapanse. Many
states like Jharkhand and Madhya Pradesh are rich in minerals like coal but lack
rural development.

States like Assam have exotic flora and fauna but lack good transport
infrastructure in the hilly regions. Places like Rajsthan have great solar and
wind energy producing potential but have a severe lack of water.

This requires competent resource planning at the national, state, regional and
local levels.

### Resources Planning in India

There are various steps to resource planning in a diverse and complex country
like India.

All those steps have been condensed into three basic categories:

**1. Identification and inventory of resources across the regions of the
country.**

**2. Evolving a planning structure endowed with appropriate technology, skill
and institutional set up for implementing resource development plans.**

**3. Matching the resource development plans with overall national development
plans.**

It can't be overstated how important technological progress is in the
development of resources. Many European powers colonised regions simply because
these regions were rich in natural resources but lacked the technological means
to make effective use of them.

Being technologically superior the colonizers asserted their supremacy over
their colonies and exploited their resources.

## Conservation of Resources

Resources are undeniably one of the biggest factors that decide the trajectory
of a country's developmental path that it will be able to tread . It dictates
how it will achieve its economic goals and how quickly.

But resources are, as previously established, not infinite. They need to be
conserved and used in a fashion which gives consideration to the need of the
future generations, that is they need to be developed **sustainably.**

> Sustainable economic development means ‘development should take place without
> damaging the environment, and development in the present should not compromise
> with the needs of the future generations.’

## Land Resources

Since we perform a ton of economic activities on land, it's a very important
resource. It also support economic activities, in the form of mining,
recreational activities etc.

India's lands are as such:

**1. Plains** : 43%

The plains give fertile soil to farm and produce food for the population and
land for industry.

**2. Mountains** : 30%

The mountains are home to many critical perennial rivers.

**3. Plateau** : 27%

It possesses rich reserves of minerals, fossil fuels and forests.

### Land Utilization

Land resources are used for the following purposes:

**1. Forests**

**2. Land not available for cultivation**

- Barren and waste land

- Land put to non-agricultural uses, e.g. buildings, roads, factories, etc.

**3. Other uncultivated land** (excluding fallow land)

- Permanent pastures and grazing land

- Land under miscellaneous tree crops groves (not included in net sown area)

- Cultruable waste land (left uncultivated for more than 5 agricultural years).

**4. Fallow lands**

- Current fallow-(left without cultivation for one or less than one agricultural
  year),

- Other than current fallow-(left uncultivated for the past 1 to 5 agricultural
  years).

**5. Net sown area** the physical extent of land on which crops are sown and
harvested is known as net sown area.

**Area sown more than once in an agricultural year plus net sown area** is known
as **gross cropped area.**

### Land Use Pattern

The use of land is determined both by:

**1. Physical factors**

Topography, climate, soil types

**2. Human factors**

Population density, technological capability, culture, traditions etc.

{{< figure align=center src="/images/geography/resources/land_use_pattern.webp" alt="Piecharts of Land Use Pattern in India" caption="Land Use Pattern in India" >}}

India has **3.28 million sq. km** of land. Though **land use data** is **only
available for 93% of that land.** This is due to encroachment of land by China
and Pakistan in Jammu & Kashmir and also because of improper land survey in
north-eastern states, except Assam.

**54%** land area is used for **cultivating**, if all the other land types
(except current fallow) are added up.

**22.5%** is covered by **forests**, which is 9.5% lower than the desired 33%
outlined in the Forest Policy Act (1951).

**3.3%** of the land is used for **grazing.**

The remaining lands are wastelands or fallow. **Waste land includes rocky, arid
and desert areas and land put to other non-agricultural uses, including
settlements, roads, railways, industry etc.**

Another _very important_ fact is that **net sown area varies wildly** across
states, for example, about **80%** of land is cultivated in **Punjab & Haryana**
while **less than 10%** of land is cultivated in **Arunachal Pradesh, Mizoram,
Manipur and Andaman Nicobar Islands.**

### Land Degradation

Our land faces serious degradation due to human activities like **deforestation,
overgrazing, and mining**, which **harm the environment** and **reduce land
productivity.**

For instance, **mining leaves scars on the landscape, while overgrazing in
states like Gujarat and Rajasthan depletes soil nutrients.** **Over-irrigation**
in places like **Punjab and Haryana** leads to salinity issues, and industrial
dust hampers water absorption in the soil.

**To combat these problems**, we need to implement solutions such as
**afforestation**, controlled grazing, and proper waste management. By adopting
these practices, we can protect our land and ensure it remains viable for future
generations.

## Classification of Soil

India has varied relief features, landforms, climatic realms and vegetation
types. These have contributed in the development of various types of soils

{{< figure align=center src="/images/geography/resources/soil_profile.webp" alt="Diagram of soil profile" caption="Soil Profile" >}}

### Alluvial Soil

- **Geographical Spread**:

    - Predominantly found in the northern plains of India.
    - Deposited by major Himalayan river systems: Indus, Ganga, Brahmaputra.
    - Extends into Rajasthan and Gujarat via a narrow corridor.
    - Present in the eastern coastal plains, especially in the deltas of
      Mahanadi, Godavari, Krishna, and Kaveri rivers.

- **Composition**:

    - Contains varying proportions of sand, silt, and clay.
    - Soil particle size increases as you move inland towards river valleys.
    - Coarse soils are common in piedmont plains such as Duars, Chos, and Terai.

- **Classification**:

    - **Old Alluvial Soil (Bangar)**: Contains more kanker nodules, less finer
      particles, and is generally less fertile.
    - **New Alluvial Soil (Khadar)**: Less kanker nodules, finer particles, more
      fertile, ideal for agriculture.

- **Fertility and Uses**:

    - Highly fertile due to balanced proportions of potash, phosphoric acid, and
      lime.
    - Suitable for growing sugarcane, paddy, wheat, and other cereal and pulse
      crops.
    - Regions with alluvial soil are often intensively cultivated and densely
      populated.

- **Challenges and Solutions**:
    - Drier areas may have more alkaline soils.
    - Can be productive with proper treatment and irrigation.

### Black Soil

- **Appearance and Name**:

    - Black in color, also known as regur or black cotton soil.
    - Ideal for growing cotton, hence the name black cotton soil.

- **Formation**:

    - Formed from climatic conditions and parent rock material.
    - Typical of the Deccan Trap (Basalt) region, originating from lava flows.
    - Covers plateaus in Maharashtra, Saurashtra, Malwa, Madhya Pradesh, and
      Chhattisgarh.
    - Extends southeast along the Godavari and Krishna valleys.

- **Composition and Properties**:

    - Made up of fine, clayey material.
    - Excellent moisture retention capacity.
    - Rich in nutrients like calcium carbonate, magnesium, potash, and lime.
    - Generally low in phosphoric content.

- **Soil Behavior**:
    - Develops deep cracks during hot weather, aiding in soil aeration.
    - Becomes sticky when wet and difficult to work with unless tilled right
      after the first rain or during the pre-monsoon period.

### Red and Yellow Soils

- **Formation and Location**:

    - Develops on crystalline igneous rocks in low rainfall areas.
    - Predominantly found in the eastern and southern parts of the Deccan
      plateau.
    - Also present in Odisha, Chhattisgarh, southern parts of the middle Ganga
      plain, and along the piedmont zone of the Western Ghats.

- **Color and Composition**:
    - Reddish color due to the diffusion of iron in crystalline and metamorphic
      rocks.
    - Appears yellow when hydrated.

### Laterite Soil

**Origin and Name**:

- Derived from the Latin word ‘later,’ meaning brick.

- **Formation and Climate**:

    - Develops under tropical and subtropical climates with alternating wet and
      dry seasons.
    - Results from intense leaching due to heavy rainfall.

- **Characteristics**:

    - Deep to very deep, acidic (pH < 6.0).
    - Generally deficient in plant nutrients.
    - Supports deciduous and evergreen forests with humus-rich soil; humus-poor
      in semi-arid environments or sparse vegetation.

- **Geographical Distribution**:

    - Predominantly in southern states, Western Ghats of Maharashtra, Odisha,
      parts of West Bengal, and North-east regions.

- **Uses and Challenges**:
    - Prone to erosion and degradation, particularly in hilly areas.
    - After adopting soil conservation techniques, it is effective for growing
      tea and coffee.
    - Red laterite soils in Tamil Nadu, Andhra Pradesh, and Kerala are suitable
      for cashew nut cultivation.

### Arid Soils

- **Color and Texture**:

    - Ranges from red to brown.
    - Generally sandy in texture.

- **Salinity and Composition**:

    - Often saline, with high salt content in some areas.
    - Common salt is extracted by evaporating water.

- **Climate and Characteristics**:

    - Found in dry climates with high temperatures.
    - Rapid evaporation leads to low humus and moisture content.

- **Soil Layers**:

    - Lower horizons contain Kankar (calcareous nodules) due to increasing
      calcium content.
    - Kankar layer restricts water infiltration.

- **Agricultural Potential**:
    - With proper irrigation, these soils can become cultivable.
    - Example: Western Rajasthan has successfully utilized these soils for
      agriculture.

### Forest Soils

- **Location**:

    - Found in hilly and mountainous areas with ample rainfall.

- **Texture**:

    - Varies by environment:
        - **Valley Sides**: Loamy and silty.
        - **Upper Slopes**: Coarse-grained.

- **Characteristics by Region**:
    - **Snow-Covered Areas (e.g., Himalayas)**: Acidic, low in humus, and prone
      to denudation.
    - **Lower Valleys and River Terraces**: Fertile soils found on alluvial
      fans.

## Soil Erosion

**The destruction of soil cover and subsequent washing down is called erosion.**

Generally, **soil erosion and soil formation go on in tandem** but sometimes
soil **erosion is accelerated by human activities** such as : deforestation,
over-grazing, construction, mining etc.

### Natural Soil Erosion

1. **Water**

- Running water cuts through clayey soils and makes them as **gullies.** These
  lands then become unfit for cultivation and they're termed **bad lands.**

- Sometimes water flows as a sheet over large areas down a slope which causes
  top soil to wash away. This is called **sheet erosion.**

2. **Wind**

- Wind blows loose soil off flat or sloping land known as **wind erosion.**

### Soil Erosion by Humans

1. **Causes**

- **Ploughing in a wrong way** i.e. **up and down the slope** form channels for
  the **quick flow of water leading to soil erosion.**

- Deforestation

- Overgrazing

2. **Solutions**

- Ploughing along the contour lines can decelerate the flow of water down the
  slopes. This is called **contour ploughing.**

- Terrace farming

- Strip cropping where strips of grass are left to grow between crops to slow
  down soil erosion by breaking the speed of winds.

- Rows of trees can be planted around farms or sandy, loose soil area. The trees
  break the wind and slow soil erosion. These are known as **shelter belts** and
  they've contributed significantly in stabilising sand dunes in western India.

## Important Events and Pledges

### Rio de Janeiro Earth Summit, 1992

In **June 1992**, more than 100 world leaders met in Rio de Janeiro, Brazil.
This was the **first international Earth summit.** They **signed the Declaration
on Global Climatic Change and Biological Diversity** to address the urgent
problems of environmental protection and socio- economic development globally.

They endorsed the global Forest Principles and **adopted Agenda 21** for
achieving Sustainable Development in the 21st century.

### Agenda 21

It's the declaration signed by world leaders in 1992 in Rio de Janeiro, Brazil.
This took place at the United Nations Conference on Environment and Development
(UNCED).

This agenda was signed with the aim to combat environmental damage, poverty
through global cooperation and mutual needs and shared responsibilities. One
major objective of the Agenda 21 is that every local government should draw its
own local Agenda 21.
