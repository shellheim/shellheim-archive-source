---
title: 'Federalism'
date: 2024-12-16T16:20:17+05:30
tags: ['Federalism', 'Civics', 'Politics', 'SST']

draft: false
---

Federalism is a system of governance where power is divided between a central
authority and constituent units (e.g., states or provinces). Common in large
democracies, federalism ensures both unity and regional diversity.

## Key Features of Federalism

- At least two levels, governing the same citizens.
- They govern the same populace with their jurisdictions defined by the
  constitution for specific legislative areas like taxation and administration.
- The fundamental provisions of the constitution cannot be unilaterally changed
  by one level of government, they require the assent of both the center and the
  states.
- Changes to federal provisions require the consent of multiple levels of
  government.
- Courts resolve disputes between levels of government.
- Sources of revenue for each level are clearly specified.

Thus, the federal system thus has dual objectives:

**1.** To safeguard and promote unity of the country

**2.** Accommodate regional diversity.

## Types of Federations

There are two types of federations based on the power dynamics between the
central formation and its constituent units.

### Coming Together Federations

- Independent states unite for mutual benefit (e.g., USA, Switzerland,
  Australia).
- Typically, there is equal power among states, and they have as strong as the
  federal government.

### Holding Together Federations

- Power is divided within a single country to maintain unity (e.g., India,
  Belgium, Spain).
- Central authority holds more power, and constituent units may have unequal
  powers.

For example, **Assam, Nagaland, Arunachal Pradesh and Mizoram** (ANAM) enjoy
special powers under certain provisions of the Constitution of India (**Article
371**) **due to their peculiar social and historical circumstances.**

## Federalism in India

### Key Constitutional Features

1.  **Two-tier System**

    Initially included Union and State governments. This was due to the **rise
    of regional parties and coalition politics** in the 1990s. Major **national
    parites had to ally with state parties**, and they bargained for more
    autonomy.

2.  **Three Lists of Subjects**

    - **Union List**: National subjects like defense, foreign affairs, currency.
    - **State List**: State-specific subjects like police, agriculture.
    - **Concurrent List**: Shared subjects like education and trade

    Union law prevails in conflicts.

3.  **Special Provisions for Some States**

    - States like Nagaland and Mizoram have unique rights due to historical or
      cultural factors.

4.  **Union Territories**:

    - Governed directly by the central government.

5.  **Judiciary**

    - In case of any dispute about the division of powers, the High Courts and
      the Supreme Court make a decision.

## Practices Strengthening Federalism in India

### Linguistic States

- Central leaders thought having states based on language will divide the
  country.
- States reorganized based on languages after independence, promoting unity and
  efficient governance.

### Language Policy

- Hindi is the one of the 22 scheduled language. Hindi speakers only make up 40%
  of the population.
- Flexible policy avoids imposition of any language on non-speakers.

### Centre-State Relations

- Evolution from central dominance to mutual respect and coalition governance
  post-1990.
- Judiciary and coalition politics have further balanced power dynamics.

## Decentralization

### Rationale

- Better local governance through closer decision-making to people.
- Constitutionally mandated Panchayats (rural) and Municipalities (urban)
  introduced in 1992.
- People have better knowledge of problems in their localities.
- They have a better understanding of where to spend money and how to manage it
  efficiently.

### Key Features

1. Mandatory local elections.
2. **Reserved seats for marginalized groups** and **one-third reservation
   women.**
3. Independent State Election Commissions.
4. Power and **revenue sharing** between state and local governments.

## Global Inspiration: Participative Democracy in Brazil

- Porto Alegre's participative budget system empowers citizens to make decisions
  about municipal spending.

- A similar model has been tried in parts of Kerala, India.

## Challenges and Achievements

- Regular elections have deepened democracy but significant powers and resources
  are yet to be fully devolved to local governments.
