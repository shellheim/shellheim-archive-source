---
title: 'Power Sharing'
date: 2024-12-15T21:54:31+05:30
tags: ['Civics', 'Democracy', 'Politics', 'Power', 'Government', 'SST']
draft: false
---

Democracies distribute power among different organs of government (legislature,
executive, judiciary) to ensure balance and prevent centralization. The concept
is explored through examples from **Belgium** and **Sri Lanka**, offering
contrasting approaches to power-sharing.

## Belgium

### Geography & Demographics

- **Small** European **country with _complex_ ethnic composition.**

- Population:
    - **59% Dutch-speaking** population live in the **Flemish region.**
    - **40% French-speaking** population in the **Wallonia region.**
    - 1% German-speaking population lives on the eastern part of the country.
    - **Brussels** has **80% French-speaking** population and **20%
      Dutch-speaking** population.

{{< figure src="/images/civics/power-sharing/belgium.webp" alt="Map of Belgium divided into linguistic regions" caption="Belgium's Linguistic Divide" >}}

### Challenges

- The **minority French-speaking** community was **relatively rich and
  powerful.** This **resulted in them being resented by** the **Dutch**
  speakers, since they got the benefits of education and economic development
  much more.
- This led to **tensions between the Dutch-speaking and French-speaking
  communities during** the **1950s and 1960s.**
- **The tension** between the two communities **was more acute in Brussels.**

- **In Brussels** the Dutch community which is in majority nationally but was a
  minority community in the capital.

### Solution

**An innovative Power-sharing model** between 1970 and 1993, they amended their
constitution four times so as to work out an arrangement

- Constitutionally mandated **equal representation of Dutch and French-speaking
  ministers in the central government.**
- **Some laws require** the **support of majority** of members from each
  linguistic group.
- **Powers decentralized** to state governments of both the regions. They are
  **not subordinate to the central government.**
- **Brussels**' has a separate government **has equal representation for both
  communities.**

The French-speaking people accepted equal representation in the Brussels
government of the Dutch-speaking people, because nationally Dutch-speaking
people gave equal representation to them in return.

- **Community government manages cultural, educational** and **language-related
  issues** for Dutch, French, and German-speaking groups.
- This **prevented civil strife**, maintained national unity and integrity.

## Sri Lanka

### Geography & Demographics

- Population

    - It has **74% Sinhala population**
    - It has a **minority Tamil population** measuring around **18%.**
    - Most Sinhala-speaking people are Buddhists. Tamils are mostly Hindus and
      Muslims.

- Tamils divided into:

    - The native **Sri Lankan Tamils** make up **13%** of the population.
    - The **Indian Tamils** make up **5%** of the population, they are
      **descendants of plantation workers.**

- The **Indian Tamil population lives in** the **center of Sri Lanka.** While
  the **Sri Lankan Tamils live in the northern to north-eastern** part of **Sri
  Lanka.**
- Christians make up **7%** of the population, these are both Sinhala and
  Tamils.

{{< figure src="/images/civics/power-sharing/sri_lanka.webp" alt="Map of Sri Lanka divided into ethnic regions" caption="Sri Lanka's Ethnic Divide" >}}

### Majoritarianism

- Sinhala community enforced policies favoring their dominance

    1. **Sinhala-only Act (1956)**: Made Sinhala the sole official language.
       This discriminated against the Tamils, who had a hard time competing in
       government exams for jobs and education.
    2. **Preferential treatment in education** and jobs for Sinhalas.
    3. Constitution mandated **state protection of Buddhism.**
    4. The buddhist, Sinhala leaders weren't sensitive enough to Tamil demands.
       Many **Tamil parties demanded for more autonomy to provinces populated by
       the Tamils.**

After a repeated attempts of Tamils for getting equal rights, they were forced
to demand a separate Tamil sate, the Tamil Eelam.

- This **resulted in Tamil alienation**, demands for autonomy, **leading to a
  civil war** which ended in 2009.

## Lessons from Belgium and Sri Lanka

- Belgium: Power-sharing fosters unity by respecting diversity.
- Sri Lanka: Majoritarianism undermines national unity and causes prolonged
  conflict.

## Why Power-sharing

1. **Prudential Reasons**:
    - Reduces conflict among social groups.
    - Ensures political stability.
2. **Moral Reasons**:
    - Upholds democratic principles by involving all groups in governance.
    - Promotes legitimacy and inclusivity.

## Forms of Power-sharing

1. **Horizontal Distribution**:

    - Among government organs (legislature, executive, judiciary).
    - Example: Checks and balances system.

2. **Vertical Distribution**:

    - Among different levels of government (central, state, local).
    - Example: Federal structure in India.

3. **Social Groups**:

    - Representation for minorities in governance.
    - Example: Reserved constituencies in India, Belgium’s community government.

4. **Political Entities**:
    - Power-sharing among political parties, pressure groups, and movements.
    - Example: Coalition governments.

## Lebanon

- Unique power-sharing based on religious sects (e.g., President must be
  Maronite Christian, Prime Minister Sunni Muslim).
- Criticism: Rigid structure limits merit-based political growth but ensures
  peace.

## Conclusion

- Power-sharing is crucial in democracies to maintain unity, avoid tyranny, and
  respect diversity.
- It balances prudential needs with moral values.
