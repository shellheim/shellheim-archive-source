---
title: 'The Making of a Global World'
date: 2025-02-06T09:08:18+05:30
tags: ['History', 'Economics', 'Globalization', 'SST']
draft: false
---

## The Making of a Global World

### Introduction

Globalization is often seen as a modern phenomenon, but its history stretches
back centuries. Human societies have always been interconnected through trade,
migration, the spread of ideas, and cultural exchanges.

As early as **3000 BC**, there was active coastal trade between the Indus valley
and West Asia.

The chapter explores the phases of globalization and its impact on economies and
societies.

## The Pre-modern World

### The Silk Routes: Early Trade Networks

- The Silk Routes connected Asia, Europe, and Africa through land and sea trade.
- Chinese silk, pottery, textiles, and spices flowed westward, while gold and
  silver moved to Asia.
- Cultural exchange happened alongside trade:
    - Buddhism spread from India via the Silk Routes.
    - Christian and Muslim preachers traveled along these routes.

### Food Travels: The Exchange of Crops

- Food is a key example of long-distance cultural exchange:
    - Noodles from China possibly became spaghetti in Italy.
    - Many common foods today such as, Potatoes, maize, tomatoes, and chillies
      arrived in Europe after Columbus' voyage to America.

All these foods were **unkown to our ancestors** until about 5 centuries ago.

The introduction of the humble **potato enabled European peasants to eat better
and live longer.** The Irish poor were so dependent on potatoes that when a
disease destroyed the potato in the **mid-1840s**, hundreds of thousands of
people died of starvation. This was known as the **Irish potato famine.**

### The Impact of Conquest and Disease

The pre-modern world shrank after **European sailors discovered sea routes to
Asia** in the **sixteenth century.**

- **Indian Ocean trade**: Before this, the Indian Ocean was a hub for trade,
  with goods, people, knowledge, and customs circulating through it, with the
  Indian subcontinent being a central point in these networks.

- **European impact on trade**: The arrival of Europeans redirected and expanded
  some of these trade flows towards Europe.

- **America’s isolation and transformation**: America had been cut off from the
  world for millions of years. After the sixteenth century, its resources like
  crops and minerals transformed global trade.

- **Precious metals and wealth**: Silver from present-day Peru and Mexico
  boosted Europe's wealth, which in turn financed trade with Asia.

In fact, **rumours spread about the** apparently **immense wealth of South
America.** These rumours fueled European expeditions in search of El Dorado, the
city of gold.

- **Spanish and Portuguese conquest**: The Spanish and Portuguese began
  conquering and colonizing America by the mid-sixteenth century.

- **The role of disease in conquest**: **The Spanish conquerors' most powerful**
  weapon **was** not firepower, but **the diseases (like smallpox) they
  carried**. Native Americans, with no immunity to these European diseases, were
  decimated by them, facilitating European conquest.

- **Migration from Europe**: Due to poverty, hunger, disease, and religious
  conflicts in Europe, many fled to America. By the eighteenth century,
  plantations in America were using enslaved Africans to grow crops like cotton
  and sugar for European markets.

- **China and India’s decline**: Until the nineteenth century, China and India
  were among the richest countries in the world and leaders in Asian trade.
  However, by the fifteenth century, China isolated itself, which led to the
  gradual shift of world trade’s center towards Europe.

- **Europe as the new center of world trade**: As China reduced its role and the
  Americas gained importance, Europe emerged as the center of global trade by
  the eighteenth century.

{{< figure src="/images/history/the-making-of-a-global-world/slaves-for-sale-new-orleans-1863.webp" alt="A cutting of the Illustrated London News from 1863, showing 8 people in top hats waiting to purchase slaves. It's a slave pen. Bunch of women with bandans on their heads standing to the side waiting to be sold."caption="Slaves awaiting sale, New Orleans, _The Illustrated London News_, 1863." >}}

## The 1800s : Industrialization & Global Economy

### Rise of a Global Agricultural Economy

- Britain’s population growth increased the nation's demand for food.
- Groups with vested interests were pressurizing the government to restrict corn
  imports. They were known as **'corn laws'.**
- Due to **pressure from urban dwellers and industrialists**, the **government
  abolished the corn laws.**
- Abolition of Corn Laws led to cheaper food imports from foreign lands but they
  also devastated local agriculture.
- **British agriculture was unable to compete with imports**, this lead to
  thousands migrating to factories in the cities or overseas.
- **As food prices fell, consumption in Britain rose.**
- From the mid-nineteenth century, **faster industrial growth in Britain also
  led to higher incomes, and therefore more food imports.**
- Large-scale grain production in the US, Russia, and Australia expanded
  agricultural trade to meet Britain's demand.

### Migration & Labor Movement

- Clearing land for agriculture required railways to link regions to ports and
  the building of new harbors.

- **Settling people on land to cultivate it needed capital and labor, with
  migration driven by labor shortages in places like America and Australia.**

- **Nearly 50 million Europeans emigrated to America and Australia in the 19th
  century**, contributing to global agricultural and economic growth.

- **By 1890, a global agricultural economy emerged**, with **food grown on large
  farms, transported by rail and ships manned by low-paid workers.**

- In West Punjab, British irrigation projects turned semi-desert into fertile
  land for wheat and cotton.

- **Global production of crops like cotton and rubber grew**, feeding industries
  like British textile mills.

- **World trade expanded 25 to 40 times between 1820 and 1914**, with 60%
  consisting of primary products like agricultural goods and minerals.

### Role of Technology in Trade

Technologies like **railways, steamships, and the telegraph were crucial for
transforming the 19th-century world**, enabling **faster and more efficient
movement of goods and information.**

A lot of this happened due to **colonizers investing in transport**, to make the
transfer of goods faster.

- **Example of Meat Trade**: **Before the 1870s, live animals were shipped from
  America to Europe**, **but many died** or became unfit for consumption during
  the long journey.

- **Refrigerated Ships**: The invention of **refrigerated ships allowed for
  slaughtering animals at the starting point** and transporting frozen meat to
  Europe, reducing costs and making meat more affordable.

{{< figure src="/images/history/the-making-of-a-global-world/shipping-beef.webp" alt="" caption="Meat being loaded on to the ship, Alexandra, The _Illustrated London News_, 1878." >}}

The **lowered cost of meat improved the European diet**, allowing the poor to
consume a more varied diet, **leading to better living conditions, social peace,
and support for imperialism.**

## Colonialism & Its Consequences

### European Colonization of Africa

- European powers divided Africa among themselves at the Berlin Conference
  (1885).
- Britain, France, Belgium, and Germany expanded their colonies.

- The U.S. also acquired former Spanish colonies in the late 1890s.

### Impact of Colonialism

- Colonialism disrupted African economies and societies.

- European explorers like Sir Henry Morton Stanley aided the conquest of Africa.

- Exploration was not just for science but linked to imperial expansion.

{{< figure src="/images/history/the-making-of-a-global-world/Africa-map-1914.png" caption="A map of Africa in 1914, with colours for the sovereign powers." >}}

### Rinderpest and Its Effects (1890s)

- **Origins and Spread:**

    - The disease, also known as cattle plague, was introduced into Africa in
      the late 1880s.
    - It arrived with infected cattle brought from British Asia to feed Italian
      troops invading Eritrea.
    - It spread rapidly across the continent, reaching the Atlantic coast by
      1892 and the Cape of Good Hope by 1897.

- **Economic and Social Impact:**

    - Rinderpest killed nearly 90% of African cattle, devastating local
      economies.
    - African communities relied heavily on cattle for food, trade, and as a
      symbol of wealth and power.
    - With the loss of cattle, food shortages and economic hardship forced many
      Africans into wage labor for European colonizers.

- **Colonial Control and Forced Labor:**
    - European plantation owners and mine operators took advantage of the crisis
      to secure cheap labor.
    - Colonial governments imposed heavy taxes and restricted land inheritance,
      ensuring Africans had no choice but to work for wages.
    - The disease played a significant role in strengthening European dominance
      over African societies.

### Indentured Labour Migration from India

- **Reasons for Migration:**

    - Economic hardships, rising land rents, and the decline of cottage
      industries in India forced many to seek work abroad.
    - British recruiters targeted laborers from eastern Uttar Pradesh, Bihar,
      central India, and Tamil Nadu.

- **Recruitment and Deception:**

    - Agents misled workers about the nature of work, wages, and living
      conditions.
    - Many migrants were unaware they would embark on long sea voyages.
    - Some were forcibly taken against their will.

- **Harsh Working Conditions:**

    - Indentured laborers worked on sugarcane, tea, and rubber plantations, as
      well as in mines and rail projects.
    - Work was physically demanding, wages were low, and living conditions were
      poor.
    - Employers punished those who did not meet quotas, and deductions were made
      from wages.

{{< figure src="/images/history/the-making-of-a-global-world/indentured-labour-in-afrca.webp" alt="" caption="Indentured laboureres photographed for identification." >}}

- **Cultural Impact and Resistance:**

    - Some workers escaped, forming new communities in the forests.
    - Over time, Indian migrants integrated with local cultures, creating unique
      cultural blends:
        - **Hosay festival** in Trinidad (inspired by Muharram).
        - **Chutney music** in Guyana and Trinidad (fusion of Indian and
          Caribbean influences).
        - **Rastafarianism** (Jamaican religious movement with Indian
          influences).

- **Abolition and Legacy:**
    - The system was abolished in 1921 after protests by Indian nationalists.
    - Many indentured laborers remained in their host countries, forming
      significant Indian diaspora communities.
    - Figures like Nobel laureate V.S. Naipaul and cricketers Shivnarine
      Chanderpaul and Ramnaresh Sarwan have Indian ancestry due to this
      migration.

### Indian Entrepreneurs and Global Trade

- **Role in Global Trade:**

    - Indian traders and moneylenders played a crucial role in financing
      colonial economies.
    - They supported export agriculture in Southeast Asia, Africa, and the
      Caribbean.

- **Key Trading Communities:**

    - **Shikaripuri Shroffs & Nattukottai Chettiars:**
        - Provided loans to farmers and plantation owners.
        - Developed a sophisticated money transfer system across vast distances.
    - **Hyderabadi Sindhi Traders:**
        - Established trading networks in port cities worldwide.
        - Sold local and imported goods to growing tourist markets.

- **Impact on Indian and Global Economy:**
    - Indian traders followed European colonizers and adapted to new markets.
    - Their financial networks helped sustain export-based agriculture.
    - Many Indian business families later became influential in international
      trade.

## The Interwar Economy

### Impact of World War I on Global Trade

- War disrupted trade, leading to economic instability.
- Britain borrowed heavily from the US, shifting global financial power to
  America.

### The Great Depression

- The stock market crash led to a global economic crisis.
- Agricultural prices collapsed, hitting Indian farmers hard.
- Unemployment soared; millions lost jobs worldwide.

### Impact on India

- Jute and wheat prices crashed; farmers faced severe debt.
- Gold exports from India helped Britain’s recovery but worsened the crisis for
  Indian peasants.
- The economic distress fueled support for Gandhi’s Civil Disobedience Movement
  (s).

## Rebuilding a Global Economy: Post-World War II

### The Bretton Woods System

- To prevent another depression, Western nations created the IMF & World Bank.
- US dollar was linked to gold, ensuring currency stability.
- Trade & investment flourished between -.

### Decolonization & Development Struggles

- Many Asian & African nations gained independence, but economies remained weak.
- The Group of (G-) demanded a New International Economic Order (NIEO) to
  correct global inequalities.

### End of Bretton Woods & Rise of Globalization

- US economic decline in the s led to the collapse of fixed exchange rates.
- Multinational corporations (MNCs) moved production to low-wage Asian
  countries.
- China & India integrated into the world economy, fueling rapid globalization.

## Conclusion

- Globalization is an ongoing historical process that shaped economies, trade,
  and societies.
- The nineteenth-century global economy expanded trade but deepened colonial
  exploitation.
- World Wars & economic crises disrupted globalization, but post-war efforts
  revived trade.
- Modern globalization emerged from these historical developments, continuing to
  reshape the world today.
