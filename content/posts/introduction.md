---
title: '📌 Introduction to the Site'
date: 2023-07-24T21:50:29+05:30
weight: 1
author: Shellheim
tags: ['intro', 'tutorial', 'first']
---

# Welcome!

This is 'The Shell Archives' which is a fancy way of saying I use this to
archive stuff related to studies.

## How to use

It's pretty simple :

1. Click on `Classes` on the homepage's nav bar.
2. Choose Class
3. Choose Subject

and have fun!
